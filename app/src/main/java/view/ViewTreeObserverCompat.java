package view;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.view.ViewTreeObserver;

/**
 * Helper for accessing features in {@link ViewTreeObserver} introduced after API
 * level 4 in a backwards compatible fashion.
 */
public final class ViewTreeObserverCompat {

    static class ViewTreeObserverCompatBaseImpl {

        public void removeOnGlobalLayoutListener(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener victim) {
            viewTreeObserver.removeGlobalOnLayoutListener(victim);
        }
    }

    @TargetApi(VERSION_CODES.JELLY_BEAN)
    static class ViewTreeObserverCompatApi16Impl extends ViewTreeObserverCompatBaseImpl {

        @Override
        public void removeOnGlobalLayoutListener(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener victim) {
            viewTreeObserver.removeOnGlobalLayoutListener(victim);
        }
    }

    static final ViewTreeObserverCompatBaseImpl IMPL;
    static {
        final int version = Build.VERSION.SDK_INT;
        if (version >= VERSION_CODES.JELLY_BEAN) {
            IMPL = new ViewTreeObserverCompatApi16Impl();
        } else {
            IMPL = new ViewTreeObserverCompatBaseImpl();
        }
    }

    /*
     * Hide the constructor.
     */
    private ViewTreeObserverCompat() {
    }

    /**
     * Remove a previously installed global layout callback
     *
     * @param victim The callback to remove
     *
     * @throws IllegalStateException If {@link ViewTreeObserver#isAlive()} returns false
     *
     * @see ViewTreeObserver#addOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener)
     */
    public static void removeOnGlobalLayoutListener(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener victim) {
        IMPL.removeOnGlobalLayoutListener(viewTreeObserver, victim);
    }
}
