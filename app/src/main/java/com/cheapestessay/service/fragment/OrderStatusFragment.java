package com.cheapestessay.service.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cheapestessay.service.BuildConfig;
import com.cheapestessay.service.R;
import com.cheapestessay.service.adapter.RecyclerviewAdapter;
import com.cheapestessay.service.model.CompletedFiles;
import com.cheapestessay.service.model.CouponCode;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.ui.neworder.CheckoutActivity;
import com.cheapestessay.service.ui.order.OrderDetailsActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.MyDownloadManager;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner {

    @Nullable
    @BindView(R.id.tv_days)
    TextViewSFDisplayBold tvDays;
    @Nullable
    @BindView(R.id.tv_hours)
    TextViewSFDisplayBold tvHours;
    @Nullable
    @BindView(R.id.tv_minutes)
    TextViewSFDisplayBold tvMinutes;
    @Nullable
    @BindView(R.id.tv_second)
    TextViewSFDisplayBold tvSecond;
    @Nullable
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @Nullable
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @Nullable
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @Nullable
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @Nullable
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @Nullable
    @BindView(R.id.tv_deadline_text)
    TextViewSFDisplayRegular tvDeadlineText;
    @Nullable
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @Nullable
    @BindView(R.id.tv_file_name)
    TextViewSFDisplayBold tvFileName;
    @Nullable
    @BindView(R.id.tv_date)
    TextViewSFDisplayRegular tvDate;
    @Nullable
    @BindView(R.id.tv_wr_id)
    TextViewSFDisplayBold tvWrId;
    @Nullable
    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @Nullable
    @BindView(R.id.tv_pay_amount)
    TextViewSFDisplayRegular tvPayAmount;
    @Nullable
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;
    @Nullable
    @BindView(R.id.tv_no_files)
    TextViewSFDisplayBold tvNoFiles;
    @Nullable
    @BindView(R.id.ll_timer)
    LinearLayout llTimer;
    @Nullable
    @BindView(R.id.ll_pay)
    LinearLayout llPay;
    @Nullable
    @BindView(R.id.ll_completed_files)
    LinearLayout llCompletedFiles;
    @Nullable
    @BindView(R.id.tv_order_status)
    TextViewSFDisplayBold tvOrderStatus;
    @Nullable
    @BindView(R.id.tv_payment_status)
    TextViewSFDisplayBold tvPaymentStatus;
    @Nullable
    @BindView(R.id.ll_status)
    LinearLayout llStatus;

    private RecyclerviewAdapter mAdapter;
    private OrderByIdModel.Data orderData;
    private List<CompletedFiles.Data> completedFiles;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_status, container, false);
        ButterKnife.bind(this, v);

        completedFiles = new ArrayList<>();

        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (orderData != null) {
            if (rvFiles != null) {
                rvFiles.setLayoutManager(new LinearLayoutManager(activity));
            }
            llStatus.setVisibility(View.VISIBLE);
            if (orderData.orderStatusName.equalsIgnoreCase(getString(R.string.processing)) ||
                    orderData.orderStatusName.equalsIgnoreCase(getString(R.string.awarded))) {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.blue_button_bg));
                }
                setTimerText();
            } else if (orderData.orderStatusName.equalsIgnoreCase(getString(R.string.completed))) {
                if (llTimer != null) {
                    llTimer.setVisibility(View.GONE);
                }
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.paid_button_bg));
                }
                llCompletedFiles.setVisibility(View.VISIBLE);
                getCompletedFiles();
            } else if (orderData.orderStatusName.equalsIgnoreCase(getString(R.string.waiting_for_payment))) {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                }
                setTimerText();
            } else {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                }
                setTimerText();
            }

            if (tvOrderStatus != null) {
                tvOrderStatus.setText(orderData.orderStatusName);
            }
            switch (orderData.status.toLowerCase()) {
                case "not paid":
                case "unpaid":
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                    llPay.setVisibility(View.VISIBLE);
                    if (tvDeadlineText != null) {
                        tvDeadlineText.setText(String.format(getString(R.string.deadline_text), orderData.dead));
                    }
                    if (tvPayAmount != null) {
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                        if (!TextUtils.isEmpty(orderData.couponCode)) {
                            tvHaveCode.setVisibility(View.GONE);
                            llCouponCode.setVisibility(View.GONE);
                            rlApplyingCode.setVisibility(View.VISIBLE);
                            tvOldTotal.setVisibility(View.VISIBLE);
                            tvCouponCode.setText(orderData.couponCode);
                            tvOldTotal.setText("$" + Utils.numberFormat2Places(orderData.subTotal));
                            tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                        }
                    }
                    break;
                case "paid":
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.paid_button_bg));
                    break;
            }
            tvPaymentStatus.setText(orderData.status);
            tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        return v;
    }

    public void setTimerText() {
        try {
            int day = orderData.days;
            int hour = orderData.hours;
            int minute = orderData.minutes;
            int second = orderData.seconds;

            if (orderData.orderStatusName.equalsIgnoreCase(getString(R.string.processing)) ||
                    orderData.orderStatusName.equalsIgnoreCase(getString(R.string.awarded))) {
                int tempSec = (day * (24 * 60 * 60)) + (hour * 60 * 60) + (minute * 60) + second;

                if (tempSec > 0) {
                    new CountDownTimer(tempSec * 1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(days);
                            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            setTimerUi((int) days, (int) hours, (int) minutes, (int) seconds);
                        }

                        @Override
                        public void onFinish() {
                        }
                    }.start();
                }
            } else {
                setTimerUi(day, hour, minute, second);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTimerUi(int day, int hour, int minute, int second) {
        llTimer.setVisibility(View.VISIBLE);

        if (tvHours != null) {
            tvHours.setText(Utils.doubleDigit(hour));
        }
        if (tvDays != null) {
            tvDays.setText(Utils.doubleDigit(day));
        }
        if (tvMinutes != null) {
            tvMinutes.setText(Utils.doubleDigit(minute));
        }
        if (tvSecond != null) {
            tvSecond.setText(Utils.doubleDigit(second));
        }
    }

    private void getCompletedFiles() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CompletedFiles> call = activity.getService().getCompletedFiles(orderData.orderId);
        call.enqueue(new Callback<CompletedFiles>() {
            @Override
            public void onResponse(Call<CompletedFiles> call, Response<CompletedFiles> response) {
                CompletedFiles model = response.body();
                if (activity.checkStatus(model)) {
                    completedFiles = model.data;
                    if (completedFiles != null && completedFiles.size() > 0) {
                        if (tvNoFiles != null) {
                            tvNoFiles.setVisibility(View.GONE);
                        }
                        if (rvFiles != null) {
                            rvFiles.setVisibility(View.VISIBLE);
                        }
                        mAdapter = new RecyclerviewAdapter((ArrayList<?>) completedFiles, R.layout.item_status_complete_files,
                                OrderStatusFragment.this);
                        rvFiles.setAdapter(mAdapter);
                    } else {
                        if (tvNoFiles != null) {
                            tvNoFiles.setVisibility(View.VISIBLE);
                        }
                        if (rvFiles != null) {
                            rvFiles.setVisibility(View.GONE);
                        }
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CompletedFiles> call, Throwable t) {
                activity.failureError("completed file failed");
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (orderData != null) {
                if (orderData.status.equalsIgnoreCase(getString(R.string.not_paid))) {
                    if (!TextUtils.isEmpty(orderData.couponCode)) {
                        tvHaveCode.setVisibility(View.GONE);
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(orderData.couponCode);
                        tvOldTotal.setText("$" + Utils.numberFormat2Places(orderData.subTotal));
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                    } else {
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                        tvOldTotal.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.GONE);
                        tvHaveCode.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Optional
    @OnClick({R.id.tv_apply, R.id.img_coupon_close, R.id.tv_coupon_remove, R.id.tv_have_code,
            R.id.tv_completed, R.id.rl_pay})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.tv_apply:
                if (TextUtils.isEmpty(etCouponcode.getText().toString())) {
                    activity.toastMessage("enter coupon code");
                    return;
                }
                applyCouponCode(true);
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                applyCouponCode(false);
                break;
            case R.id.rl_pay:
                Intent i = new Intent(activity, CheckoutActivity.class);
                i.putExtra(Constants.ORDER_DATA, orderData);
                startActivity(i);
                activity.openToTop();
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_completed:
                activity.toastMessage("Completed");
                break;
        }
    }

    public String getCouponCode() {
        return etCouponcode.getText().toString().trim();
    }

    public void applyCouponCode(final boolean isApply) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().applyCode(isApply ? getCouponCode() : "x", activity.getAccessToken(),
                orderData.orderId, activity.getUserId());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (model != null) {
                    if (activity.checkStatus(model)) {
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(getCouponCode());
                        tvOldTotal.setText("$" + Utils.numberFormat2Places(model.data.subTotal));
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(model.data.total));
                        orderData.total = model.data.total;
                        orderData.subTotal = model.data.subTotal;
                        orderData.couponDiscount = model.data.couponDiscount;
                        orderData.couponCode = getCouponCode();
                    } else {
                        tvOldTotal.setVisibility(View.GONE);
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(model.data.total));
                        rlApplyingCode.setVisibility(View.GONE);
                        tvHaveCode.setVisibility(View.VISIBLE);
                        etCouponcode.setText("");
                        orderData.total = model.data.total;
                        orderData.subTotal = model.data.subTotal;
                        orderData.couponDiscount = model.data.couponDiscount;
                        orderData.couponCode = "";
                        if (!isApply) {
                            activity.toastMessage("Coupon code successfully removed.");
                        } else {
                            activity.failureError(model.msg);
                        }
                    }
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("coupon code failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        ButterKnife.bind(this, view);

        final CompletedFiles.Data item = completedFiles.get(position);
        if (tvFileName != null) {
            tvFileName.setText(item.fileName);
        }
        if (tvDate != null) {
            tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "dd/MM/yyyy", item.dateAdded));
        }
        if (tvWrId != null) {
            tvWrId.setText(TextUtils.isEmpty(item.writerId) ? "" : item.writerId);
        }
        if (imgMenu != null) {
            imgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showOptionDialog(item);
                }
            });
        }
    }

    private void showOptionDialog(final CompletedFiles.Data item) {
        final Dialog dialog = new Dialog(activity, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llView = dialog.findViewById(R.id.ll_view);
        View llDownload = dialog.findViewById(R.id.ll_download);
        View llEmail = dialog.findViewById(R.id.ll_email);
        View llShare = dialog.findViewById(R.id.ll_share);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, false, false);
            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, true, false, false);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, false, true);
            }
        });

        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, true, false);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final CompletedFiles.Data item, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/" + activity.getString(R.string.app_name));
        if (!folder.exists())
            folder.mkdir();

        final File file = new File(folder, item.fileName);
        if (!file.exists()) {
            activity.showProgress();
            String url = "https://cdn.cheapestessay.com/" + item.src;

            MyDownloadManager downloadManager = new MyDownloadManager(activity)
                    .setDownloadUrl(url)
                    .setTitle(item.fileName)
                    .setDestinationUri(file)
                    .setDownloadCompleteListener(new MyDownloadManager.DownloadCompleteListener() {
                        @Override
                        public void onDownloadComplete() {
                            activity.hideProgress();
                            showOutput("Download complete", isDownload, file, isEmailShare, isShare);
                        }

                        @Override
                        public void onDownloadFailure() {
                            activity.hideProgress();
                            activity.toastMessage("Download failed");
                        }
                    });
            downloadManager.startDownload();
        } else {
            showOutput("Already Downloaded", isDownload, file, isEmailShare, isShare);
        }
    }

    private void showOutput(String message, boolean isDownload, File file, boolean isEmailShare, boolean isShare) {
        if (!isDownload) {
            if (isShare || isEmailShare)
                shareFile(file, isEmailShare);
            else
                viewFile(file);
        } else {
            activity.toastMessage(message);
        }
    }

    private void viewFile(File file) {
        try {
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String mime = "*/*";
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (mimeTypeMap.hasExtension(extension))
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            intent.setDataAndType(uri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
        } catch (Exception e) {
            activity.toastMessage("No application available to view this type of file");
            e.printStackTrace();
        }
    }

    private void shareFile(File file, boolean isEmailShare) {
        Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
        Intent intentShareFile = new Intent(isEmailShare ? Intent.ACTION_SENDTO : Intent.ACTION_SEND);
        intentShareFile.setType("*/*");
        if (isEmailShare) {
            intentShareFile.setData(Uri.parse("mailto:"));
            intentShareFile.putExtra(Intent.EXTRA_EMAIL, "");
        }
        intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
        activity.startActivity(Intent.createChooser(intentShareFile, "Share File"));
    }

    private void checkPermission(final CompletedFiles.Data item, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        downloadFile(item, isDownload, isEmailShare, isShare);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        activity.toastMessage("give storage permission first");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}
