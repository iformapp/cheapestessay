package com.cheapestessay.service.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.CouponCode;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.ui.neworder.CheckoutActivity;
import com.cheapestessay.service.ui.order.OrderDetailsActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPaymentFragment extends BaseFragment {

    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_order_date)
    TextViewSFDisplayRegular tvOrderDate;
    @BindView(R.id.tv_deadline)
    TextViewSFDisplayRegular tvDeadline;
    @BindView(R.id.tv_paypal)
    TextViewSFDisplayRegular tvPaypal;
    @BindView(R.id.tv_sales_id)
    TextViewSFDisplayRegular tvSalesId;
    @BindView(R.id.tv_subtotal)
    TextViewSFDisplayRegular tvSubtotal;
    @BindView(R.id.label_coupon_code)
    TextViewSFDisplayRegular labelCouponCode;
    @BindView(R.id.tv_discount)
    TextViewSFDisplayRegular tvDiscount;
    @BindView(R.id.tv_redeem)
    TextViewSFDisplayRegular tvRedeem;
    @BindView(R.id.tv_total)
    TextViewSFDisplayBold tvTotal;
    @BindView(R.id.ll_payment_details)
    LinearLayout llPaymentDetails;
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @BindView(R.id.ll_checkout_pay)
    LinearLayout llCheckoutPay;
    @BindView(R.id.tv_pay_text)
    TextViewSFTextRegular tvPayText;
    @BindView(R.id.tv_pay_amount)
    TextViewSFDisplayRegular tvPayAmount;
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;
    @BindView(R.id.rl_discount)
    RelativeLayout rlDiscount;
    @BindView(R.id.rl_redeem)
    RelativeLayout rlRedeem;
    @BindView(R.id.rl_sales_id)
    RelativeLayout rlSalesId;
    @BindView(R.id.line_sales_id)
    ImageView lineSalesId;
    @BindView(R.id.tv_lifetime_discount)
    TextViewSFDisplayRegular tvLifetimeDiscount;
    @BindView(R.id.rl_lifetime_discount)
    RelativeLayout rlLifetimeDiscount;

    private OrderByIdModel.Data orderData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_payment, container, false);
        ButterKnife.bind(this, v);

        init();
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (orderData != null && !orderData.status.equalsIgnoreCase(getString(R.string.paid))) {
                llPaymentDetails.setVisibility(View.GONE);
                llCheckoutPay.setVisibility(View.VISIBLE);
                tvPayText.setText(String.format(getString(R.string.payment_not_received), orderData.dead));
                if (!TextUtils.isEmpty(orderData.couponCode)) {
                    tvHaveCode.setVisibility(View.GONE);
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(orderData.couponCode);
                    tvOldTotal.setText(orderData.subTotal);
                    tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                } else {
                    tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                    tvOldTotal.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.GONE);
                    tvHaveCode.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void init() {
        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (orderData != null) {
            if (orderData.status.equalsIgnoreCase(getString(R.string.paid))) {
                llPaymentDetails.setVisibility(View.VISIBLE);
                llCheckoutPay.setVisibility(View.GONE);
                tvOrderNo.setText(orderData.orderNumber);
                tvOrderDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MM-dd-yyyy hh:mm:ss a", orderData.orderDate));
                tvDeadline.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MM-dd-yyyy hh:mm:ss a", orderData.orderDeadline));
                if (!TextUtils.isEmpty(orderData.salesId)) {
                    tvSalesId.setText(orderData.salesId);
                } else {
                    rlSalesId.setVisibility(View.GONE);
                    lineSalesId.setVisibility(View.GONE);
                }
                labelCouponCode.setText("");
                tvSubtotal.setText("$" + Utils.numberFormat2Places(orderData.subTotal));
                if (Double.parseDouble(Utils.priceWithout$(orderData.discountBenifit)) > 0) {
                    rlLifetimeDiscount.setVisibility(View.VISIBLE);
                    tvLifetimeDiscount.setText(Utils.priceWith$(orderData.discountBenifit));
                }
                if (Double.parseDouble(Utils.priceWithout$(orderData.couponDiscount)) > 0) {
                    tvDiscount.setText("$" + orderData.couponDiscount);
                } else {
                    rlDiscount.setVisibility(View.GONE);
                }
                if (Double.parseDouble(Utils.priceWithout$(orderData.redeem)) > 0) {
                    tvRedeem.setText("$" + Utils.numberFormat2Places(Utils.priceWithout$(orderData.redeem)));
                } else {
                    rlRedeem.setVisibility(View.GONE);
                }
                tvTotal.setText("$" + Utils.numberFormat2Places(orderData.total));
            } else {
                llPaymentDetails.setVisibility(View.GONE);
                llCheckoutPay.setVisibility(View.VISIBLE);
                tvPayText.setText(String.format(getString(R.string.payment_not_received), orderData.dead));
                tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                if (!TextUtils.isEmpty(orderData.couponCode)) {
                    tvHaveCode.setVisibility(View.GONE);
                    llCouponCode.setVisibility(View.GONE);
                    rlApplyingCode.setVisibility(View.VISIBLE);
                    tvOldTotal.setVisibility(View.VISIBLE);
                    tvCouponCode.setText(orderData.couponCode);
                    tvOldTotal.setText("$" + Utils.numberFormat2Places(orderData.subTotal));
                    tvPayAmount.setText("$" + Utils.numberFormat2Places(orderData.total));
                }
                tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
    }

    @OnClick({R.id.tv_apply, R.id.img_coupon_close, R.id.tv_coupon_remove, R.id.rl_pay, R.id.tv_have_code})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.tv_apply:
                if (activity.isEmpty(etCouponcode.getText().toString())) {
                    activity.toastMessage("enter coupon code");
                    return;
                }
                applyCouponCode(true);
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                applyCouponCode(false);
                break;
            case R.id.rl_pay:
                Intent i = new Intent(activity, CheckoutActivity.class);
                i.putExtra(Constants.ORDER_DATA, orderData);
                startActivity(i);
                activity.openToTop();
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
        }
    }

    public String getCouponCode() {
        return etCouponcode.getText().toString().trim();
    }

    public void applyCouponCode(final boolean isApply) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CouponCode> call = activity.getService().applyCode(isApply ? getCouponCode() : "x", activity.getAccessToken(),
                orderData.orderId, activity.getUserId());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (model != null) {
                    if (activity.checkStatus(model)) {
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(getCouponCode());
                        tvOldTotal.setText("$" + Utils.numberFormat2Places(model.data.subTotal));
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(model.data.total));
                        orderData.total = model.data.total;
                        orderData.subTotal = model.data.subTotal;
                        orderData.couponDiscount = model.data.couponDiscount;
                        orderData.couponCode = getCouponCode();
                    } else {
                        tvOldTotal.setVisibility(View.GONE);
                        tvPayAmount.setText("$" + Utils.numberFormat2Places(model.data.total));
                        rlApplyingCode.setVisibility(View.GONE);
                        tvHaveCode.setVisibility(View.VISIBLE);
                        etCouponcode.setText("");
                        orderData.total = model.data.total;
                        orderData.subTotal = model.data.subTotal;
                        orderData.couponDiscount = model.data.couponDiscount;
                        orderData.couponCode = "";
                        if (!isApply) {
                            activity.toastMessage("Coupon code successfully removed.");
                        } else {
                            activity.failureError(model.msg);
                        }
                    }
                    ((OrderDetailsActivity) activity).setOrderData(orderData);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                activity.failureError("coupon code failed");
            }
        });
    }
}
