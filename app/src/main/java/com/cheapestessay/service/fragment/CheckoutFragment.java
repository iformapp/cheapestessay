package com.cheapestessay.service.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.IntercomData;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.ui.neworder.CheckoutActivity;
import com.cheapestessay.service.ui.neworder.PayPalPaymentActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.checkbox.CheckBoxSFTextRegular;
import com.cheapestessay.service.util.edittext.EditTextSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutFragment extends BaseFragment {

    private final int REQ_PAYMENT_CODE = 9001;

    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_deadline)
    TextViewSFDisplayRegular tvDeadline;
    @BindView(R.id.tv_subtotal)
    TextViewSFDisplayRegular tvSubtotal;
    @BindView(R.id.tv_discount)
    TextViewSFDisplayRegular tvDiscount;
    @BindView(R.id.tv_redeem)
    TextViewSFDisplayRegular tvRedeem;
    @BindView(R.id.tv_total)
    TextViewSFDisplayBold tvTotal;
    @BindView(R.id.ll_checkout)
    LinearLayout llCheckout;
    @BindView(R.id.ll_redeem_view)
    LinearLayout llRedeemView;
    @BindView(R.id.rl_discount)
    RelativeLayout rlDiscount;
    @BindView(R.id.rl_redeem)
    RelativeLayout rlRedeem;
    @BindView(R.id.tv_balance)
    TextViewSFDisplayBold tvBalance;
    @BindView(R.id.tv_return_redeem)
    TextViewSFDisplayRegular tvReturnRedeem;
    @BindView(R.id.et_redeem_amount)
    EditTextSFDisplayBold etRedeemAmount;
    @BindView(R.id.tv_lifetime_discount)
    TextViewSFDisplayRegular tvLifetimeDiscount;
    @BindView(R.id.rl_lifetime_discount)
    RelativeLayout rlLifetimeDiscount;
    @BindView(R.id.chk_tnc)
    CheckBoxSFTextRegular chkTnc;

    private boolean isVisa;
    private OrderByIdModel.Data orderData;
    private UserModel.Data userData;
    private double redeemAmount;
    private double userBalance;
    private String lifeTimeDiscount = "$0";

    public static CheckoutFragment newInstanace(boolean isVisa) {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IS_VISA, isVisa);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_checkout, container, false);
        ButterKnife.bind(this, v);

        if (getArguments() != null) {
            isVisa = getArguments().getBoolean(Constants.IS_VISA);
        }

        if (getActivity() != null) {
            orderData = ((CheckoutActivity) getActivity()).getOrderData();
        }

        userData = Preferences.getUserData(activity);

        if (orderData != null) {
            userBalance = Double.parseDouble(Utils.priceWithout$(userData.balance));
            tvOrderNo.setText(orderData.orderNumber);
            tvDeadline.setText(orderData.deadline);
            tvSubtotal.setText(Utils.priceWith$(orderData.subTotal));
            refreshView();
        }

        ClickableSpan tncClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                activity.redirectUsingCustomTab(getString(R.string.autologin_url, Constants.TERMS_USE, activity.getAccessToken()));
            }
        };

        ClickableSpan ppClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                activity.redirectUsingCustomTab(getString(R.string.autologin_url, Constants.PRIVACY, activity.getAccessToken()));
            }
        };

        Utils.makeLinks(chkTnc, new String[]{"Terms of Use", "Privacy Policy"}, new ClickableSpan[]{tncClick, ppClick});
        return v;
    }

    private void refreshView() {
        if (orderData != null) {
            if (Double.parseDouble(Utils.priceWithout$(orderData.couponDiscount)) > 0) {
                rlDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(Utils.priceWith$(Utils.numberFormat2Places(orderData.couponDiscount)));
            }
            if (userData != null && userBalance > 0) {
                llRedeemView.setVisibility(View.VISIBLE);
                tvBalance.setText(Utils.priceWith$(Utils.numberFormat2Places(userData.balance)));
            }
            if (Double.parseDouble(Utils.priceWithout$(orderData.benifitValue)) > 0) {
                rlLifetimeDiscount.setVisibility(View.VISIBLE);
                double totalBenifit = Utils.getDouble(orderData.total) * (Utils.getDouble(orderData.benifitValue) / 100);
                lifeTimeDiscount = Utils.priceWith$(Utils.numberFormat2Places(totalBenifit));
                tvLifetimeDiscount.setText(lifeTimeDiscount);
            }
            tvTotal.setText(Utils.priceWith$(Utils.numberFormat2Places(orderData.total)));
        }
    }

    @OnClick({R.id.tv_return_redeem, R.id.btn_checkout, R.id.tv_apply_redeem})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_return_redeem:
                removeRedeem();
                break;
            case R.id.tv_apply_redeem:
                if (!activity.isEmpty(getRedeemValue())) {
                    makeRedeem();
                } else {
                    activity.validationError("enter your redeem amount");
                }
                break;
            case R.id.btn_checkout:
                if (chkTnc.isChecked()) {
                    String url = activity.getString(R.string.paypal_url, activity.getUserId(), orderData.orderId,
                            activity.getAccessToken(), String.valueOf(redeemAmount), isVisa ? "card" : "paypal");

                    Intent i = new Intent(activity, PayPalPaymentActivity.class);
                    i.putExtra(Constants.PAYPAL_URL, url);
                    startActivityForResult(i, REQ_PAYMENT_CODE);
                } else {
                    activity.validationError("Please check privacy policy");
                }
                break;
        }
    }

    private String getTotal() {
        return Utils.priceWithout$(tvTotal.getText().toString().trim());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_PAYMENT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getIntercomData();
            }
        }
    }

    private void sendDataToIntercom(IntercomData.Data data) {
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withCustomAttribute("account_age", data.accountAge)
                .withCustomAttribute("ip_address", data.ipAddress)
                .withCustomAttribute("city", data.city)
                .withCustomAttribute("intercomm_total_payment", data.intercommTotalPayment)
                .withCustomAttribute("current_orders_count", data.currentOrdersCount)
                .withCustomAttribute("refunded_orders_count", data.refundedOrdersCount)
                .withCustomAttribute("all_orders_count", data.allOrdersCount)
                .withCustomAttribute("current_orders", data.currentOrders)
                .withCustomAttribute("last_order_date", data.currentOrders)
                .withCustomAttribute("last_paid_order_date", data.currentOrders)
                .withCustomAttribute("paypal_email", data.currentOrders)
                .withCustomAttribute("group", data.currentOrders)
                .withCustomAttribute("account_level", data.currentOrders)
                .withCustomAttribute("email", data.currentOrders)
                .withCustomAttribute("name", data.currentOrders)
                .withCustomAttribute("country", data.currentOrders)
                .withCustomAttribute("timezone", data.currentOrders)
                .withCustomAttribute("referral_url", data.currentOrders)
                .withCustomAttribute("lead_category", data.currentOrders)
                .build();
        Intercom.client().updateUser(userAttributes);
    }

    private void getIntercomData() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<IntercomData> call = activity.getService().getIntercomData(activity.getUserId());
        call.enqueue(new Callback<IntercomData>() {
            @Override
            public void onResponse(Call<IntercomData> call, Response<IntercomData> response) {
                IntercomData intercomData = response.body();
                if (intercomData != null && activity.checkStatus(intercomData)) {
                    sendDataToIntercom(intercomData.data);
                    redirectToOrderActivity();
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<IntercomData> call, Throwable t) {
                //activity.failureError("intercom data failed");
                redirectToOrderActivity();
                activity.hideProgress();
            }
        });
    }

    private void redirectToOrderActivity() {
        userData.balance = String.valueOf(userBalance);
        Preferences.saveUserData(activity, userData);
        activity.gotoMainActivity(Constants.TAB_ORDER);
    }

    public void updateOrderStatus(String nonce) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().brainTreeTrx(nonce, activity.getAccessToken(), orderData.orderId,
                String.valueOf(redeemAmount), activity.getUserId(), getTotal());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (activity.checkStatus(response.body())) {
                        userData.balance = String.valueOf(userBalance);
                        Preferences.saveUserData(activity, userData);
                        activity.toastMessage(response.body().msg);
                        activity.gotoMainActivity(Constants.TAB_ORDER);
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("update password failed");
            }
        });
    }

    public String getRedeemValue() {
        return etRedeemAmount.getText().toString().trim();
    }

    public void makeRedeem() {
        redeemAmount = Double.parseDouble(Utils.priceWithout$(getRedeemValue()));

        if (redeemAmount == 0) {
            activity.validationError("Redeem amount must be greater than 0");
            return;
        }

        if (redeemAmount > userBalance) {
            activity.validationError("You can not redeem amount more than your balance");
            return;
        }

        double total = Double.parseDouble(getTotal());
        if (redeemAmount > total) {
            activity.validationError("You can not redeem amount more than total amount");
            return;
        }

        Utils.hideSoftKeyboard(activity);
        llRedeemView.setVisibility(View.GONE);
        rlRedeem.setVisibility(View.VISIBLE);
        tvRedeem.setText(Utils.priceWith$(Utils.numberFormat2Places(getRedeemValue())));
        tvTotal.setText(Utils.priceWith$(Utils.numberFormat2Places(total - redeemAmount)));
        userBalance = userBalance - redeemAmount;
        ((CheckoutActivity) activity).isRedeemed(true);
    }

    public void removeRedeem() {
        etRedeemAmount.setText("");
        llRedeemView.setVisibility(View.VISIBLE);
        rlRedeem.setVisibility(View.GONE);
        tvTotal.setText(Utils.priceWith$(Utils.numberFormat2Places(orderData.total)));
        userBalance = userBalance + redeemAmount;
        redeemAmount = 0;
        ((CheckoutActivity) activity).isRedeemed(false);
    }
}
