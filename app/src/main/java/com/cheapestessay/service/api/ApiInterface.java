package com.cheapestessay.service.api;

import com.cheapestessay.service.model.Balance;
import com.cheapestessay.service.model.CompletedFiles;
import com.cheapestessay.service.model.CouponCode;
import com.cheapestessay.service.model.FileUpload;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.IntercomData;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.model.OrderModel;
import com.cheapestessay.service.model.PriceCalculate;
import com.cheapestessay.service.model.ReferralLink;
import com.cheapestessay.service.model.TypesModel;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.util.Constants;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(Constants.URL)
    Call<TypesModel> getType(@Query("method") String method);

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<Object> login(@Field("user_email") String email, @Field("user_password") String password,
                          @Field("device_token") String token, @Field("device_type") String type,
                          @Field("timezone") String timezone, @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.REGISTER)
    Call<Object> register(@Field("email") String email, @Field("firstname") String firstname,
                             @Field("lastname") String lastname, @Field("password") String password,
                             @Field("tel_prefix") String dialCode, @Field("mobile") String mobile,
                             @Field("timezone") String timezone, @Field("country") String country,
                             @Field("countryname") String countryname, @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.REGISTER_STEP1)
    Call<Object> registerStep1(@Field("email") String email, @Field("password") String password,
                                  @Field("tel_prefix") String dialCode, @Field("mobile") String mobile,
                                  @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.REGISTER_STEP2)
    Call<Object> registerStep2(@Field("firstname") String firstname, @Field("lastname") String lastname,
                                  @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.SOCIAL_LOGIN)
    Call<Object> socialLogin(@Field("email") String email, @Field("firstname") String firstname,
                             @Field("lastname") String lastname, @Field("password") String password,
                             @Field("tel_prefix") String dialCode, @Field("mobile") String mobile,
                             @Field("timezone") String timezone, @Field("iso_code") String isoCode,
                             @Field("countryname") String countryname, @Field("device_id") String deviceId,
                             @Field("sn_id") String snId);

    @FormUrlEncoded
    @POST(Constants.SAVE_PROFILE)
    Call<UserModel> saveProfile(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                @Field("first_name") String firstName, @Field("last_name") String last_name,
                                @Field("telephone") String telephone, @Field("telephone_prefix") String telephonePrefix,
                                @Field("iso_code") String iso_code, @Field("countryname") String countryname);

    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD)
    Call<GeneralModel> forgotPassword(@Field("user_email") String email);

    @FormUrlEncoded
    @POST(Constants.SET_PASSWORD)
    Call<GeneralModel> setPassword(@Field("accesstoken") String accesstoken, @Field("new_password") String newPassword,
                                   @Field("current_password") String currentPassword, @Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.RESET_PASSWORD)
    Call<GeneralModel> resetPassword(@Field("accesstoken") String accesstoken, @Field("user_password") String userPassword,
                                     @Field("otp") String otp);

    @FormUrlEncoded
    @POST(Constants.SEND_FEEDBACK)
    Call<GeneralModel> sendFeedback(@Field("userid") String userid, @Field("message") String message);

    @FormUrlEncoded
    @POST(Constants.GET_ORDER)
    Call<OrderModel> getOrder(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                              @Field("stat") String stat);

    @FormUrlEncoded
    @POST(Constants.DELETE_ORDER)
    Call<GeneralModel> deleteOrder(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                   @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.ORDER_DETAIL)
    Call<OrderByIdModel> getOrderById(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                      @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.ORDER_TRX)
    Call<GeneralModel> updateOrderStatus(@Field("trxID") String trxID, @Field("accesstoken") String accesstoken,
                                         @Field("order_id") String orderId, @Field("redeem_amount") String redeemAmount,
                                         @Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.INTERCOM_DATA)
    Call<IntercomData> getIntercomData(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.BRAIN_TREE_TRX)
    Call<GeneralModel> brainTreeTrx(@Field("nonce") String nonce, @Field("accesstoken") String accesstoken,
                                    @Field("order_id") String orderId, @Field("redeem_amount") String redeemAmount,
                                    @Field("user_id") String userId, @Field("amount") String amount);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<PriceCalculate> getPrice(@Field("deadlineType") String deadlineType, @Field("deadlineValue") String deadlineValue,
                                  @Field("writerLevelId") int writerLevelId, @Field("page") String page,
                                  @Field("extraSendItToEmail") int extraSendItToEmail, @Field("extraAbstract") int extraAbstract,
                                  @Field("extraTurnintin") int extraTurnintin);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<PriceCalculate> getPrice(@Field("deadlineType") String deadlineType, @Field("deadlineValue") String deadlineValue,
                                  @Field("writerLevelId") int writerLevelId, @Field("page") String page,
                                  @Field("extraSendItToEmail") int extraSendItToEmail, @Field("extraAbstract") int extraAbstract,
                                  @Field("extraTurnintin") int extraTurnintin, @Field("serviceTypeId") String serviceTypeId,
                                  @Field("paperTypeId") String paperTypeId, @Field("paperTypeOther") String paperTypeOther,
                                  @Field("subjectId") String subjectId, @Field("subjectOther") String subjectOther,
                                  @Field("chart") String chart, @Field("source") String source,
                                  @Field("slide") String slide, @Field("formatStyleId") String formatStyleId,
                                  @Field("formatStyleOther") String formatStyleOther, @Field("disciplineId") String disciplineId,
                                  @Field("writer") String writer, @Field("topic") String topic,
                                  @Field("spacing") String spacing, @Field("orderDetail") String orderDetail,
                                  @Field("writer_id") String writer_id);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE)
    Call<Object> getPrice(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATE_EDIT)
    Call<Object> getPriceForEditOrder(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(Constants.UPDATE_ORDER)
    Call<Object> updateOrder(@FieldMap Map<String, String> map);

    @Multipart
    @POST(Constants.FILE_UPLOAD)
    Call<FileUpload> uploadMaterial(@Part MultipartBody.Part[] file, @Part("category") RequestBody category,
                                    @Part("order_id") RequestBody order_id, @Part("user_id") RequestBody user_id,
                                    @Part("accesstoken") RequestBody accesstoken);

    @Multipart
    @POST(Constants.NEW_FILE_UPLOAD)
    Call<FileUpload> uploadNewFile(@Part MultipartBody.Part[] file, @Part("category") RequestBody category,
                                   @Part("order_id") RequestBody order_id, @Part("user_id") RequestBody user_id,
                                   @Part("accesstoken") RequestBody accesstoken);

    @FormUrlEncoded
    @POST(Constants.DELETE_UPLOAD)
    Call<Object> deleteFile(@Field("filename") String filename, @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.SET_ORDER_STATUS)
    Call<Object> saveOrder(@Field("accesstoken") String accesstoken, @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.APPLY_COUPON)
    Call<CouponCode> applyCode(@Field("couponCode") String couponCode, @Field("accesstoken") String accesstoken,
                               @Field("order_id") String orderId, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.GET_BALANCE)
    Call<UserModel> getBalance(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.GET_BALANCE_HISTORY)
    Call<Balance> getBalanceHistory(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.GET_REFERRAL_LINK)
    Call<ReferralLink> getReferralLink(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.GET_COMPLETED_FILES)
    Call<CompletedFiles> getCompletedFiles(@Field("order_id") String orderId);
}
