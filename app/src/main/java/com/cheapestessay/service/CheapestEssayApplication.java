package com.cheapestessay.service;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.cheapestessay.service.api.ApiClient;
import com.cheapestessay.service.api.ApiInterface;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.TypesModel;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.woopra.Woopra;
import com.cheapestessay.service.woopra.WoopraTracker;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheapestEssayApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        printHashKey();

        WoopraTracker tracker = Woopra.getInstance(this).getTracker("cheapestessay.com");
        tracker.setIdleTimeout(30);
        tracker.setPingEnabled(true);

        Intercom.initialize(this, "android_sdk-a90ddbab259e17039b94f33dbacc86ccb7dc783b", "je6f9lsz");
        //Intercom.initialize(this, "android_sdk-409f1cca72c430a48b720749a857a7c31a5da596", "je6f9lsz");  // OLD KEY
        Registration registration = Registration.create();
        if (Preferences.getUserData(this) != null) {
            registration.withEmail(Preferences.getUserData(this).email);
            registration.withUserId(Preferences.getUserData(this).userId);
            Intercom.client().registerIdentifiedUser(registration);
        } else {
            Intercom.client().registerUnidentifiedUser();
        }

        getTypes(Constants.PAPER_TYPES);
        getTypes(Constants.DISCIPLIN_TYPES);
        getTypes(Constants.FORMATED_STYLE_TYPES);
        getTypes(Constants.SUBJECTS_TYPES);
        getTypes(Constants.CATEGORY_TYPES);

        TypesModel typesModel = new TypesModel();
        List<TypesModel.Data> list = new ArrayList<>();
        TypesModel.Data data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.any_writer);
        data.academicLevelId = "any_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.top_10_writer);
        data.academicLevelId = "top_10_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.my_old_writer);
        data.academicLevelId = "my_previous_writer";
        list.add(data);

        typesModel.data = list;

        Preferences.saveTypes(getApplicationContext(), typesModel.data, Constants.ACADEMIC_TYPES);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void getTypes(final String types) {
        if (!isNetworkConnected())
            return;

        Call<TypesModel> call = getService().getType(types);
        call.enqueue(new Callback<TypesModel>() {
            @Override
            public void onResponse(@NonNull Call<TypesModel> call, @NonNull Response<TypesModel> response) {
                TypesModel typesModel = response.body();
                if (typesModel != null) {
                    if (checkStatus(typesModel)) {
                        Log.e("Response ==>  ", types);
                        Preferences.saveTypes(getApplicationContext(), typesModel.data, types);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<TypesModel> call, @NonNull Throwable t) {
            }
        });
    }

    public boolean checkStatus(GeneralModel model) {
        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag != null) {
            return model.flag.equals("1");
        }
        return false;
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash Key: ", hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Hash Key: ", e.getMessage());
        } catch (Exception e) {
            Log.e("Hash Key: ", e.getMessage());
        }
    }
}
