package com.cheapestessay.service.tooltip;

import android.widget.PopupWindow;

/**
 * Listener that is called when this Tooltip is dismissed.
 */
public interface OnDismissListener extends PopupWindow.OnDismissListener {
}
