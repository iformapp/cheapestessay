package com.cheapestessay.service.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.Deadline;
import com.cheapestessay.service.ui.home.HomeActivity;
import com.cheapestessay.service.ui.neworder.NewOrderActivity;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeadlineAdapter extends RecyclerView.Adapter<DeadlineAdapter.SimpleViewHolder> {

    private ArrayList<Deadline> mDataset;
    private Context context;
    private int selectedPosition;
    private boolean isHomePage;

    public DeadlineAdapter(Context context, ArrayList<Deadline> mDataset, boolean isHomePage) {
        this.context = context;
        this.mDataset = mDataset;
        this.isHomePage = isHomePage;
        selectedPosition = mDataset.size() - 1;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deadline, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Deadline item = mDataset.get(position);

        holder.tvType.setText(item.deadlineType);
        holder.tvValue.setText(item.deadlineValue);

        if (selectedPosition == position) {
            if (position == 0) {
                holder.llView.setBackground(ContextCompat.getDrawable(context, R.drawable.left_corner_select));
            } else if (position == mDataset.size() - 1) {
                holder.llView.setBackground(ContextCompat.getDrawable(context, R.drawable.right_corner_select));
            } else {
                holder.llView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }
            holder.tvType.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            if (position == 0) {
                holder.llView.setBackground(ContextCompat.getDrawable(context, R.drawable.left_corner_unselect));
            } else if (position == mDataset.size() - 1) {
                holder.llView.setBackground(ContextCompat.getDrawable(context, R.drawable.right_corner_unselect));
            } else {
                holder.llView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }
            if (item.deadlineType.equalsIgnoreCase(context.getString(R.string.hours))) {
                holder.tvType.setTextColor(ContextCompat.getColor(context, R.color.reset_pw_btn));
            } else {
                holder.tvType.setTextColor(ContextCompat.getColor(context, R.color.lightgreen));
            }
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.black));
        }

        holder.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = position;
                if (isHomePage)
                    ((HomeActivity) context).onDeadlineSelect(item);
                else
                    ((NewOrderActivity) context).onDeadlineSelect(item);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_value)
        TextViewSFTextRegular tvValue;
        @BindView(R.id.tv_type)
        TextViewSFTextRegular tvType;
        @BindView(R.id.ll_view)
        LinearLayout llView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
