package com.cheapestessay.service.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.OrderModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.ui.order.OrderDetailsActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapter extends RecyclerSwipeAdapter<OrderAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<OrderModel.Data> mDataset;
    private EditOrderListner editOrderListner;

    public OrderAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_items, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<OrderModel.Data> objects) {
        this.mDataset = objects;
        notifyDataSetChanged();
    }

    public interface EditOrderListner {
        void onEditOrder(String orderId);
    }

    public void setEditOrderListner(EditOrderListner editOrderListner) {
        this.editOrderListner = editOrderListner;
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder viewHolder, int position) {
        final OrderModel.Data item = mDataset.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        viewHolder.rlItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, OrderDetailsActivity.class);
                i.putExtra(Constants.ORDER_ID, item.orderId);
                mContext.startActivity(i);
                ((BaseActivity) mContext).openToLeft();
            }
        });

        viewHolder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(item.orderId, viewHolder.swipeLayout, viewHolder.getAdapterPosition());
            }
        });

        viewHolder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editOrderListner != null) {
                    editOrderListner.onEditOrder(item.orderId);
                }
            }
        });
        viewHolder.tvOrderid.setText(item.orderNumber);
        viewHolder.tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MMM d, yyyy", item.orderDate));
        viewHolder.tvPrice.setText(item.total);
        if (item.paymentStatus.toLowerCase().equalsIgnoreCase(mContext.getString(R.string.unpaid))) {
            viewHolder.swipeLayout.setSwipeEnabled(true);
            viewHolder.llDelete.setVisibility(View.VISIBLE);
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.reset_pw_btn));
            viewHolder.tvStatusWaiting.setVisibility(View.VISIBLE);
            viewHolder.tvStatus.setVisibility(View.GONE);
            viewHolder.tvTimerStatus.setVisibility(View.GONE);
            viewHolder.tvStatusWaiting.setText(item.orderStatusName);
        } else {
            viewHolder.llDelete.setVisibility(View.GONE);
            viewHolder.tvStatusWaiting.setVisibility(View.GONE);
            if (item.orderStatusName.equalsIgnoreCase(mContext.getString(R.string.processing)) ||
                    item.orderStatusName.equalsIgnoreCase(mContext.getString(R.string.awarded))) {
                viewHolder.swipeLayout.setSwipeEnabled(true);
                viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                viewHolder.setTimerText(item);
                viewHolder.tvStatus.setVisibility(View.VISIBLE);
                viewHolder.tvTimerStatus.setVisibility(View.GONE);
            } else if (item.orderStatusName.equalsIgnoreCase(mContext.getString(R.string.completed))) {
                viewHolder.swipeLayout.setSwipeEnabled(false);
                viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.lightgreen));
                viewHolder.tvTimerStatus.setText(item.orderStatusName);
                viewHolder.tvStatus.setVisibility(View.GONE);
                viewHolder.tvTimerStatus.setVisibility(View.VISIBLE);
            } else {
                viewHolder.swipeLayout.setSwipeEnabled(false);
                viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.lightgreen));
                viewHolder.tvTimerStatus.setText(item.orderStatusName);
                viewHolder.tvStatus.setVisibility(View.GONE);
                viewHolder.tvTimerStatus.setVisibility(View.VISIBLE);
            }
        }
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_itemview)
        RelativeLayout rlItemView;
        @BindView(R.id.ll_edit)
        LinearLayout llEdit;
        @BindView(R.id.ll_delete)
        LinearLayout llDelete;
        @BindView(R.id.divider_line)
        View dividerLine;
        @BindView(R.id.tv_orderid)
        TextViewSFDisplayBold tvOrderid;
        @BindView(R.id.tv_date)
        TextViewSFDisplayRegular tvDate;
        @BindView(R.id.tv_status)
        TextViewSFDisplayRegular tvStatus;
        @BindView(R.id.tv_price)
        TextViewSFDisplayBold tvPrice;
        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.tv_status_waiting)
        TextViewSFDisplayRegular tvStatusWaiting;
        @BindView(R.id.tv_timer_status)
        TextViewSFDisplayRegular tvTimerStatus;
        CountDownTimer timer;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setTimerText(OrderModel.Data item) {
            try {
                int day = Integer.parseInt(item.days);
                int hour = Integer.parseInt(item.hours);
                int minute = Integer.parseInt(item.minutes);
                int second = Integer.parseInt(item.seconds);
                int tempSec = (day * (24 * 60 * 60)) + (hour * 60 * 60) + (minute * 60) + second;

                if (tempSec > 0) {
                    if (timer == null) {
                        timer = new CountDownTimer(tempSec * 1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                                long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(days);
                                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                                String timerText = Utils.doubleDigit(days) + " days: " + Utils.doubleDigit(hours) + " h: " +
                                        Utils.doubleDigit(minutes) + " m: " + Utils.doubleDigit(seconds) + " s";
                                tvStatus.setText(timerText);
                            }

                            @Override
                            public void onFinish() {
                            }
                        }.start();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteDialog(final String orderId, final SwipeLayout swipeLayout, final int position) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(String.format(mContext.getString(R.string.delete_order_text), orderId)));

        tvCancel.setText(mContext.getString(R.string.no));
        tvChatnow.setText(mContext.getString(R.string.yes));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteOrders(orderId, swipeLayout, position);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void deleteOrders(String orderId, final SwipeLayout swipeLayout, final int position) {
        final BaseActivity activity = (BaseActivity) mContext;
        if (activity == null || !activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().deleteOrder(activity.getAccessToken(), activity.getUserId(), orderId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(@NonNull Call<GeneralModel> call, @NonNull Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (activity.checkStatus(response.body())) {
                        mItemManger.removeShownLayouts(swipeLayout);
                        mDataset.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mDataset.size());
                        mItemManger.closeAllItems();
                        ((BaseActivity) mContext).toastMessage("Deleted");
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(@NonNull Call<GeneralModel> call, @NonNull Throwable t) {
                activity.failureError("delete Order failed");
            }
        });
    }
}
