package com.cheapestessay.service.adapter.binder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.cheapestessay.service.BuildConfig;
import com.cheapestessay.service.R;
import com.cheapestessay.service.model.UserFiles;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.MyDownloadManager;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesBinder extends ItemBinder<UserFiles, FilesBinder.FilesViewHolder> {

    private Context mContext;

    @Override
    public FilesViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        mContext = parent.getContext();
        return new FilesViewHolder(inflater.inflate(R.layout.item_list_files, parent, false));
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof UserFiles;
    }

    @Override
    public void bind(FilesViewHolder holder, final UserFiles item) {
        holder.tvFileName.setText(item.filename);
        holder.tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy", "dd/MM/yyyy", item.fileUploadSimpleDate));
        holder.tvOwner.setText("By me");
        holder.imgView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.eye_gray));

        holder.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionDialog(item);
            }
        });
    }

    static class FilesViewHolder extends BaseViewHolder<UserFiles> {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.tv_date)
        TextViewSFDisplayRegular tvDate;
        @BindView(R.id.tv_owner)
        TextViewSFDisplayRegular tvOwner;
        @BindView(R.id.img_view)
        ImageView imgView;

        public FilesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showOptionDialog(final UserFiles userFiles) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llView = dialog.findViewById(R.id.ll_view);
        View llDownload = dialog.findViewById(R.id.ll_download);
        View llEmail = dialog.findViewById(R.id.ll_email);
        View llShare = dialog.findViewById(R.id.ll_share);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(userFiles, false, false, false);
            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(userFiles, true, false, false);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(userFiles, false, false, true);
            }
        });

        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(userFiles, false, true, false);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final UserFiles userFiles, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/" + mContext.getString(R.string.app_name));
        if (!folder.exists())
            folder.mkdir();

        final File file = new File(folder, userFiles.filename);
        if (!file.exists()) {
            ((BaseActivity) mContext).showProgress();

            MyDownloadManager downloadManager = new MyDownloadManager(mContext)
                    .setDownloadUrl(userFiles.filepath)
                    .setTitle(userFiles.filename)
                    .setDestinationUri(file)
                    .setDownloadCompleteListener(new MyDownloadManager.DownloadCompleteListener() {
                        @Override
                        public void onDownloadComplete() {
                            ((BaseActivity) mContext).hideProgress();
                            showOutput("Download complete", isDownload, file, isEmailShare, isShare);
                        }

                        @Override
                        public void onDownloadFailure() {
                            ((BaseActivity) mContext).hideProgress();
                            ((BaseActivity) mContext).toastMessage("Download failed");
                        }
                    });
            downloadManager.startDownload();
        } else {
            showOutput("Already Downloaded", isDownload, file, isEmailShare, isShare);
        }
    }

    private void showOutput(String message, boolean isDownload, File file, boolean isEmailShare, boolean isShare) {
        if (!isDownload) {
            if (isShare || isEmailShare)
                shareFile(file, isEmailShare);
            else
                viewFile(file);
        } else {
            ((BaseActivity) mContext).toastMessage(message);
        }
    }

    private void viewFile(File file) {
        try {
            Uri uri =  Uri.fromFile(file);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            String mime = "*/*";
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (mimeTypeMap.hasExtension(extension))
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            intent.setDataAndType(uri,mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            mContext.startActivity(intent);
        } catch (Exception e) {
            ((BaseActivity) mContext).toastMessage("No application available to view this type of file");
            e.printStackTrace();
        }
    }

    private void shareFile(File file, boolean isEmailShare) {
        Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", file);
        Intent intentShareFile = new Intent(isEmailShare ? Intent.ACTION_SENDTO : Intent.ACTION_SEND);
        intentShareFile.setType("*/*");
        if (isEmailShare) {
            intentShareFile.setData(Uri.parse("mailto:"));
            intentShareFile.putExtra(Intent.EXTRA_EMAIL, "");
        }
        intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
        mContext.startActivity(Intent.createChooser(intentShareFile, "Share File"));
    }

    private void checkPermission(final UserFiles userFiles, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        Dexter.withActivity((Activity) mContext)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        downloadFile(userFiles, isDownload, isEmailShare, isShare);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        ((BaseActivity) mContext).toastMessage("give storage permission first");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}