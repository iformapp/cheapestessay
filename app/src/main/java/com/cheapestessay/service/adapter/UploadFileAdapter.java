package com.cheapestessay.service.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.FileUpload;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.ui.neworder.NewOrderActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFileAdapter extends RecyclerView.Adapter<UploadFileAdapter.SimpleViewHolder> {

    private List<FileUpload.Data> mDataset;
    private Context context;
    private BaseActivity activity;

    public UploadFileAdapter(Context context) {
        this.context = context;
        activity = ((BaseActivity) context);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uploaded_files, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<FileUpload.Data> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, int position) {
        viewHolder.tvFileName.setText(mDataset.get(position).filename);

        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile(viewHolder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void deleteFile(final int position) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Log.e("Delete File Url = > ", Constants.BASE_URL + Constants.DELETE_UPLOAD);
        String filepath = mDataset.get(position).hashedFileName;

        Call<Object> call = activity.getService().deleteFile(filepath, mDataset.get(position).orderId);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String jsonResposne = new Gson().toJson(response.body());
                Log.e("delete file response", jsonResposne);

                if (activity.checkStatus(jsonResposne)) {
                    mDataset.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, mDataset.size());
                    try {
                        FileUpload fileUpload = activity.getModel(jsonResposne, FileUpload.class);
                        ((NewOrderActivity) context).setFileAdapter(fileUpload.data);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ((NewOrderActivity) context).setFileAdapter(null);
                    }
                }

                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                activity.failureError("delete file failed");
            }
        });
    }
}
