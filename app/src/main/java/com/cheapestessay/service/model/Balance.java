package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Balance extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("current_wallet_balance")
        public String currentWalletBalance;
        @Expose
        @SerializedName("transactionAmount")
        public String transactionamount;
        @Expose
        @SerializedName("journal_text")
        public String journalText;
        @Expose
        @SerializedName("transactionText")
        public String transactiontext;
        @Expose
        @SerializedName("date")
        public String date;
    }
}
