package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IntercomData extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {

        @Expose
        @SerializedName("lead_category")
        public String leadCategory;
        @Expose
        @SerializedName("referral_url")
        public String referralUrl;
        @Expose
        @SerializedName("timezone")
        public String timezone;
        @Expose
        @SerializedName("country")
        public String country;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("account_level")
        public String accountLevel;
        @Expose
        @SerializedName("group")
        public String group;
        @Expose
        @SerializedName("paypal_email")
        public String paypalEmail;
        @Expose
        @SerializedName("last_paid_order_date")
        public String lastPaidOrderDate;
        @Expose
        @SerializedName("last_order_date")
        public String lastOrderDate;
        @Expose
        @SerializedName("current_orders")
        public String currentOrders;
        @Expose
        @SerializedName("all_orders_count")
        public String allOrdersCount;
        @Expose
        @SerializedName("refunded_orders_count")
        public String refundedOrdersCount;
        @Expose
        @SerializedName("current_orders_count")
        public String currentOrdersCount;
        @Expose
        @SerializedName("intercomm_total_payment")
        public String intercommTotalPayment;
        @Expose
        @SerializedName("city")
        public String city;
        @Expose
        @SerializedName("ip_address")
        public String ipAddress;
        @Expose
        @SerializedName("account_age")
        public String accountAge;
    }
}
