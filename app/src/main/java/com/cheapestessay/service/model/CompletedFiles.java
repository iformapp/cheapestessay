package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CompletedFiles extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("acct_id")
        public String acctId;
        @Expose
        @SerializedName("customer_approved_status")
        public String customerApprovedStatus;
        @Expose
        @SerializedName("send_customer_by_mail_time")
        public String sendCustomerByMailTime;
        @Expose
        @SerializedName("send_customer_by_mail")
        public String sendCustomerByMail;
        @Expose
        @SerializedName("approved_by_support_date_added")
        public String approvedBySupportDateAdded;
        @Expose
        @SerializedName("approved_by_support")
        public String approvedBySupport;
        @Expose
        @SerializedName("file_word_count")
        public String fileWordCount;
        @Expose
        @SerializedName("is_completed")
        public String isCompleted;
        @Expose
        @SerializedName("date_added")
        public String dateAdded;
        @Expose
        @SerializedName("obj_id")
        public String objId;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("comment")
        public String comment;
        @Expose
        @SerializedName("file_name")
        public String fileName;
        @Expose
        @SerializedName("src")
        public String src;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("attachment_id")
        public String attachmentId;
        @Expose
        @SerializedName("writer_id")
        public String writerId;
    }
}
