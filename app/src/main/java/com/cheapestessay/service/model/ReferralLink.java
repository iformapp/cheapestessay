package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferralLink extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("referral_link")
        public String referralLink;
        @Expose
        @SerializedName("coupon_code")
        public String couponCode;
    }
}
