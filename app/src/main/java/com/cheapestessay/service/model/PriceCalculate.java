package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceCalculate extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("source")
        public String source;
        @Expose
        @SerializedName("pages")
        public String pages;
        @Expose
        @SerializedName("topic")
        public String topic;
        @Expose
        @SerializedName("subject")
        public String subject;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("deadline")
        public String deadline;
    }
}
