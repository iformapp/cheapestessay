package com.cheapestessay.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Redeem extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("current_balance")
        public double currentBalance;
        @Expose
        @SerializedName("cost_value")
        public double costValue;
    }
}
