package com.cheapestessay.service.model;

public class Deadline {

    public String deadlineType;
    public String deadlineValue;

    public Deadline(String deadlineType, String deadlineValue) {
        this.deadlineType = deadlineType;
        this.deadlineValue = deadlineValue;
    }
}
