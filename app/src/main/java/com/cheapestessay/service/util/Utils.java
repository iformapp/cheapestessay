package com.cheapestessay.service.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Utils {

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void makeLinks(CheckBox checkBox, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(checkBox.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = checkBox.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        checkBox.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        checkBox.setMovementMethod(LinkMovementMethod.getInstance());
        checkBox.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void openSoftKeyboard(Activity activity, View view) {
        if (activity == null)
            return;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String numberFormat(String number) {
        try {
            Double d = Double.parseDouble(number);
            NumberFormat nf = new DecimalFormat("#.####");
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String numberFormat2Places(Object number) {
        try {
            Double d = Double.parseDouble(String.valueOf(number));
            NumberFormat nf = new DecimalFormat("0.00");
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(number);
    }

    public static String doubleDigit(double number) {
        String s = numberFormat(String.valueOf(number));
        if (s.length() == 1) {
            s = "0" + s;
        } else if (s.length() > 3) {
            s = s.substring(0, 2) + "..";
        }
        return s;
    }

    public static String doubleDigit(String number) {
        String s = numberFormat(number);
        if (s.length() == 1) {
            s = "0" + s;
        } else if (s.length() > 3) {
            s = s.substring(0, 2) + "..";
        }
        return s;
    }

    public static double getDouble(Object number) {
        String s = String.valueOf(number);
        return Double.parseDouble(priceWithout$(s));
    }

    public static String changeDateFormat(String source, String target, String dateString) {
        SimpleDateFormat input = new SimpleDateFormat(source);
        SimpleDateFormat output = new SimpleDateFormat(target);
        try {
            Date date = input.parse(dateString);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String priceWith$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "$0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return "$" + price.trim();
    }

    public static String priceWithout$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return price.trim();
    }

    public static String getFileExtFromBytes(File f) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            byte[] buf = new byte[5]; //max ext size + 1
            fis.read(buf, 0, buf.length);
            StringBuilder builder = new StringBuilder(buf.length);
            for (int i=1;i<buf.length && buf[i] != '\r' && buf[i] != '\n';i++) {
                builder.append((char)buf[i]);
            }
            return builder.toString().toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     * @param param
     * @return
     */
    public static String encode(String param) {
        try {
            return URLEncoder.encode(param, "utf-8");
        } catch (Exception e) {
            // will not throw an exception since utf-8 is supported.
        }
        return param;
    }

    /**
     *
     * @param fristKey
     * @param secondKey
     * @return
     */
    public static String getUUID(String fristKey, String secondKey) {
        long mostSigBits = fristKey.hashCode();
        long leastSigBits = secondKey.hashCode();
        UUID generateUUID = new UUID(mostSigBits, leastSigBits);
        String result = generateUUID.toString();
        return result.replace("-", "");
    }
}
