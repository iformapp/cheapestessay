package com.cheapestessay.service.util;

public class Constants {

    //API URL PARAMS
    public static final String BASE_URL = "https://www.cheapestessay.com/";
    public static final String URL = "api_android/webservice.php";
    public static final String FILE_PATH = BASE_URL + "uploads/customers_files/";
    public static final String LOGIN = URL + "?method=login";
    public static final String REGISTER = URL + "?method=signup";
    public static final String REGISTER_STEP1 = URL + "?method=create_account_step_1";
    public static final String REGISTER_STEP2 = URL + "?method=create_account_step_2";
    public static final String SOCIAL_LOGIN = URL + "?method=snlogin";
    public static final String SAVE_PROFILE = URL + "?method=saveProfile";
    public static final String SET_PASSWORD = URL + "?method=setPasswordOnLoggedin";
    public static final String RESET_PASSWORD = URL + "?method=setpassword";
    public static final String SEND_FEEDBACK = URL + "?method=sendFeedback";
    public static final String GET_ORDER = URL + "?method=getOrders";
    public static final String DELETE_ORDER = URL + "?method=deleteOrder";
    public static final String ORDER_DETAIL = URL + "?method=getOrdersById";
    public static final String ORDER_TRX = URL + "?method=orderTrx";
    public static final String INTERCOM_DATA = URL + "?method=getIntercommData";
    public static final String BRAIN_TREE_TRX = URL + "?method=brainTreeTrx";
    public static final String FORGOT_PASSWORD = URL + "?method=forgotPassword";
    public static final String PRICE_CALCULATE = URL + "?method=setOrderOptionAll";
    public static final String PRICE_CALCULATE_EDIT = URL + "?method=calculatePrice";
    public static final String UPDATE_ORDER = URL + "?method=updateOrder";
    public static final String FILE_UPLOAD = URL + "?method=uploadMaterial";
    public static final String NEW_FILE_UPLOAD = URL + "?method=uploadNewFile";
    public static final String DELETE_UPLOAD = URL + "?method=deleteMaterial";
    public static final String DELETE_ALL_FILES = URL + "?method=deleteUploadedFiles";
    public static final String SET_ORDER_STATUS = URL + "?method=setOrderStatus";
    public static final String APPLY_COUPON = URL + "?method=updateDisc";
    public static final String MAKE_REDEEM = URL + "?method=makeRedeem";
    public static final String REMOVE_REDEEM = URL + "?method=removeRedeem";
    public static final String GET_BALANCE = URL + "?method=getBalance";
    public static final String GET_BALANCE_HISTORY = URL + "?method=getBalanceHistory";
    public static final String GET_REFERRAL_LINK = URL + "?method=getReferralLink";
    public static final String GET_COMPLETED_FILES = URL + "?method=getCompletedAttachments";

    public static final String REFUND = "money-back-guarantee.php";
    public static final String PRIVACY = "privacy-policy.php";
    public static final String REVISION = "revision-policy.php";
    public static final String TERMS_USE = "terms-of-use.php";
    public static final String DISCLAIMER = "disclaimer.php";
    public static final String WEBSITE = "index.php";
    public static final String FAQS = "faqs.php";
    public static final String SERVICES = "services.php";
    public static final String PRICES = "price.php";
    public static final String DISCOUNT = "discount.php";

    //Api parameter
    public static final String PAPER_TYPES = "paperTypes";
    public static final String ACADEMIC_TYPES = "getAcademicTypes";
    public static final String DISCIPLIN_TYPES = "getDisciplines";
    public static final String FORMATED_STYLE_TYPES = "getFormatedStyle";
    public static final String SUBJECTS_TYPES = "subjects";
    public static final String CATEGORY_TYPES = "getCategory";

    public static final int TAB_HOME = 0;
    public static final int TAB_CHAT = 1;
    public static final int TAB_PLUS = 2;
    public static final int TAB_ORDER = 3;
    public static final int TAB_PROFILE = 4;

    public static final String SFTEXT_REGULAR = "font/SanFranciscoText-Regular.otf";
    public static final String SFTEXT_BOLD = "font/SanFranciscoText-Bold.otf";
    public static final String SFDISPLAY_BOLD = "font/SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "font/SF-Pro-Display-Regular.otf";

    public static final String FROM_LOGIN = "from login";
    public static final String MEDAL_LEVEL = "medal_level";
    public static final String IS_VISA = "isVisa";
    public static final String IS_LOGIN = "isLogin";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String POLICY_URL = "policy_url";
    public static final String USER_DATA = "user_data";
    public static final String ORDER_DATA = "order_data";
    public static final String ORDER_ID = "order_id";
    public static final String EDIT_ORDER_ID = "edit_order_id";
    public static final String SCREEN_NAME = "screen_name";
    public static final String DEADLINE_TYPE = "deadlineType";
    public static final String DEADLINE_VALUE = "deadlineValue";
    public static final String WRITER_LEVEL_ID = "writerLevelId";
    public static final String PAGE = "page";
    public static final String PAYPAL_URL = "paypal_url";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final int COLLEGE_ID = 3;
    public static final int BACHELOR_ID = 1;
    public static final int MASTER_ID = 2;

    public static final int WRITING_ID = 3;
    public static final int EDITING_ID = 1;
    public static final int POWERPOINT_ID = 2;

    public static final int REQUEST_CODE_IMAGE = 23;

    public static final String W_TRACK_ENDPOINT = "http://www.woopra.com/track/ce/";
    public static final String W_IDENTIFY_ENDPOINT = "http://www.woopra.com/track/identify/";
    public static final String W_PING_ENDPOINT = "http://www.woopra.com/track/ping/";
    public static final String APP_KEY = "Woopra_android";
    public static final String COOKIE_KEY = "Woopra_android_cookie";
    public static final String NOT_SET = "NOT_SET";
    public static final String EVENT_NAME = "AppView";
    public static final String EVENT_SCREEN = "view";
    public static final String EVENT_TITLE = "title";

    public enum FilterType {
        ALL("all"),
        CURRENT("current"),
        UNPAID("unpaid"),
        COMPLETED("completed"),
        REFUNDED("refunded");

        String type;

        FilterType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }
}
