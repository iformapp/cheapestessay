package com.cheapestessay.service.ui.policy;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.adapter.RecyclerviewAdapter;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PolicyActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.rv_policy)
    RecyclerView rvPolicy;

    private RecyclerviewAdapter mAdapter;
    private ArrayList<String> arrayList;
    private ArrayList<String> urlList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        ButterKnife.bind(this);

        arrayList = fillData();
        urlList = urlData();

        tvToolbarTitle.setText(getString(R.string.policy).toUpperCase());
        tvRight.setText(getString(R.string.chat));

        rvPolicy.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerviewAdapter(arrayList, R.layout.item_policy, this);
        rvPolicy.setAdapter(mAdapter);
    }

    @OnClick({R.id.ll_back, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                openIntercomChat();
                break;
        }
    }

    @Override
    public void bindView(View view, final int position) {
        TextView textView = view.findViewById(R.id.tv_policy_title);
        View itemView = view.findViewById(R.id.rl_view);
        textView.setText(arrayList.get(position));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectUsingCustomTab(getString(R.string.autologin_url, urlList.get(position), getAccessToken()));
            }
        });
    }

    public ArrayList<String> fillData() {
        arrayList = new ArrayList<>();
        arrayList.add(getString(R.string.refund_policy));
        arrayList.add(getString(R.string.privacy_policy));
        arrayList.add(getString(R.string.revision_policy));
        arrayList.add(getString(R.string.terms_of_use));
        arrayList.add(getString(R.string.disclaimer));
        arrayList.add(getString(R.string.website));
        arrayList.add(getString(R.string.faqs));
        arrayList.add(getString(R.string.services));
        arrayList.add(getString(R.string.prices));
        return arrayList;
    }

    public ArrayList<String> urlData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(Constants.REFUND);
        arrayList.add(Constants.PRIVACY);
        arrayList.add(Constants.REVISION);
        arrayList.add(Constants.TERMS_USE);
        arrayList.add(Constants.DISCLAIMER);
        arrayList.add(Constants.WEBSITE);
        arrayList.add(Constants.FAQS);
        arrayList.add(Constants.SERVICES);
        arrayList.add(Constants.PRICES);
        return arrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Policy Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open policy screen");
        setTracker(event);
    }
}
