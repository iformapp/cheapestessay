package com.cheapestessay.service.ui.profile;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ccp.CountryCodePicker;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFTextBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.btn_right)
    TextViewSFTextBold btnRight;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;
    @BindView(R.id.tv_email)
    TextViewSFTextRegular tvEmail;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;

    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.edit_profile));

        userData = Preferences.getUserData(this);
        if (userData == null) {
            finish();
            return;
        }

        etFirstname.setText(userData.firstName);
        etLastname.setText(userData.lastName);

        if (userData.email != null) {
            tvEmail.setText(userData.email);
        }

        if (userData.mobile != null) {
            etMobile.setText(userData.mobile);
            if (userData.dialcode != null && !isEmpty(userData.dialcode)) {
                String code = userData.dialcode.replace("+", "");
                tvPhonePrefix.setText("+" + code);
                if (isEmpty(userData.isoCode)) {
                    int countryCode = Integer.parseInt(code);
                    ccp.setCountryForPhoneCode(countryCode);
                } else {
                    ccp.setCountryForNameCode(userData.isoCode);
                }
            }
        }

        ccp.registerCarrierNumberEditText(etMobile);
        addTextChangeEvent(etFirstname, etLastname, etMobile);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                btnRight.setVisibility(View.VISIBLE);
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public String getFirstName() {
        return etFirstname.getText().toString();
    }

    public String getLastName() {
        return etLastname.getText().toString();
    }

    public String getMobile() {
        return etMobile.getText().toString();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public String getCountryName() {
        return ccp.getSelectedCountryName().toLowerCase();
    }

    public String getCountryISOCode() {
        return ccp.getSelectedCountryNameCode().toLowerCase();
    }

    public void saveProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserModel> call = getService().saveProfile(getAccessToken(), getUserId(), getFirstName(), getLastName(),
                getMobile(), getMobilePrefix(), getCountryISOCode(), getCountryName());
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel userModel = response.body();
                if (userModel != null) {
                    if (checkStatus(userModel)) {
                        if (userModel.data != null) {
                            Preferences.saveUserData(EditProfileActivity.this, userModel.data);
                            toastMessage(userModel.msg);
                            btnRight.setVisibility(View.GONE);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("edit profile failed");
            }
        });
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (btnRight.getVisibility() == View.GONE) {
                btnRight.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick({R.id.ll_back, R.id.btn_right, R.id.tv_changepassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.btn_right:
                if (validData()) {
                    saveProfile();
                }
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
        }
    }

    public boolean validData() {
        if (isEmpty(getFirstName())) {
            validationError("Enter Name");
            return false;
        }

        if (isEmpty(getMobile())) {
            validationError("Enter Mobile no");
            return false;
        }

        if (!ccp.isValidFullNumber()) {
            validationError("Enter Valid Mobile no");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Edit Profile Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open edit profile screen");
        setTracker(event);
    }
}
