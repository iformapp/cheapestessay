package com.cheapestessay.service.ui.neworder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.fragment.CheckoutFragment;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckoutActivity extends BaseActivity {

    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private ViewPagerAdapter adapter;
    private OrderByIdModel.Data orderData;
    private boolean isRedeemApplied;
    private int[] tabIcons = {
            R.drawable.visa,
            R.drawable.paypal
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        tvRight.setText(getString(R.string.chat));

        if (getIntent() != null) {
            orderData = (OrderByIdModel.Data) getIntent().getSerializableExtra(Constants.ORDER_DATA);
        }

        if (orderData != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setupViewPager(viewPager);

                    tabLayout.setupWithViewPager(viewPager);
                    setupTabIcons();
                }
            }, 200);
        }
    }

    public OrderByIdModel.Data getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderByIdModel.Data orderData) {
        this.orderData = orderData;
    }

    private void setupTabIcons() {
        int tabCount = adapter.getCount();

        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                ImageView myCustomIcon = (ImageView) LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.custom_tab_icon, null);
                myCustomIcon.setImageDrawable(ContextCompat.getDrawable(this, tabIcons[i]));
                tab.setCustomView(myCustomIcon);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(CheckoutFragment.newInstanace(true), "Visa");
        adapter.addFrag(CheckoutFragment.newInstanace(false), "Paypal");
        viewPager.setAdapter(adapter);
    }

    @OnClick({R.id.ll_back, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                openIntercomChat();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    public void isRedeemed(boolean isRedeemApplied) {
        this.isRedeemApplied = isRedeemApplied;
    }

    @Override
    public void onBackPressed() {
        if (isRedeemApplied) {
            showDialog("Changes won't be save");
        } else {
            showDialog("You are about to exit Checkout");
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                        finishToBottom();
                    }
                });

        builder.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Checkout Screen");
        event.setProperty(Constants.EVENT_TITLE, "User Open Checkout Screen");
        setTracker(event);
    }
}
