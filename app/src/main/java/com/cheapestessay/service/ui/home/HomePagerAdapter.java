package com.cheapestessay.service.ui.home;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;

import java.util.ArrayList;

public class HomePagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<HomePagerModel> arrayList;

    HomePagerAdapter(Context mContext, ArrayList<HomePagerModel> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.home_page_item, collection, false);

        ImageView imgIcon = v.findViewById(R.id.img_icon);
        final TextView tvTitle = v.findViewById(R.id.tv_title);
        final TextView tvDetails = v.findViewById(R.id.tv_details);

        HomePagerModel items = arrayList.get(position);
        imgIcon.setImageDrawable(ContextCompat.getDrawable(mContext, items.icon));
        tvTitle.setText(items.title);
        tvDetails.setText(items.details);
        tvDetails.setPaintFlags(tvDetails.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String accesstoken = ((BaseActivity) mContext).getAccessToken();
                if (tvTitle.getText().toString().equalsIgnoreCase(mContext.getString(R.string.guarantee_text))) {
                    ((BaseActivity) mContext).redirectUsingCustomTab(mContext.getString(R.string.autologin_url, Constants.REFUND, accesstoken));
                } else if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.read_more))) {
                    ((BaseActivity) mContext).redirectUsingCustomTab(mContext.getString(R.string.autologin_url, Constants.PRIVACY, accesstoken));
                } else if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.chat_now))) {
                    ((BaseActivity) mContext).openIntercomChat();
                } else if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.offer_details))) {
                    ((BaseActivity) mContext).openIntercomChat();
                } else if (tvTitle.getText().toString().equalsIgnoreCase(mContext.getString(R.string.plagiarism_text))) {
                    ((BaseActivity) mContext).redirectUsingCustomTab(mContext.getString(R.string.autologin_url, Constants.PRIVACY, accesstoken));
                } else if (tvTitle.getText().toString().equalsIgnoreCase(mContext.getString(R.string.cheapest_text))) {
                    ((BaseActivity) mContext).redirectUsingCustomTab(mContext.getString(R.string.autologin_url, Constants.PRICES, accesstoken));
                }
            }
        });

        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.93f;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
