package com.cheapestessay.service.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ccp.CountryCodePicker;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.woopra.Woopra;
import com.cheapestessay.service.woopra.WoopraEvent;
import com.cheapestessay.service.woopra.WoopraTracker;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSignupNewActivity extends BaseActivity {

    private static final int LOGIN = 0;
    private static final int SIGNUP = 1;

    @BindView(R.id.tab_login)
    SegmentedButton tabLogin;
    @BindView(R.id.tab_signup)
    SegmentedButton tabSignup;
    @BindView(R.id.segmentLoginGroup)
    SegmentedButtonGroup segmentLoginGroup;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.rl_login)
    RelativeLayout rlLogin;
    @BindView(R.id.et_s_email)
    EditTextSFTextRegular etSEmail;
    @BindView(R.id.et_phone)
    EditTextSFTextRegular etPhone;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.et_s_password)
    EditTextSFTextRegular etSPassword;
    @BindView(R.id.rl_signup)
    RelativeLayout rlSignup;
    @BindView(R.id.default_fb_login)
    LoginButton defaultFbLogin;
    @BindView(R.id.default_twitter_login)
    TwitterLoginButton defaultTwitterLogin;

    private boolean isFromLogin = false;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private static final String TWITTER_KEY = "<Your Twitter Key Here>";
    private static final String TWITTER_SECRET = "<Your Twitter Secret Here>";
    private int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up_new);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();

        callbackManager = CallbackManager.Factory.create();
        initFacebook();
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        // For Google Login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (getIntent() != null) {
            isFromLogin = getIntent().getBooleanExtra(Constants.FROM_LOGIN, true);
        }

        if (isFromLogin) {
            rlLogin.setVisibility(View.VISIBLE);
            rlSignup.setVisibility(View.GONE);
        } else {
            rlLogin.setVisibility(View.GONE);
            rlSignup.setVisibility(View.VISIBLE);
        }

        segmentLoginGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                if (position == LOGIN) {
                    isFromLogin = true;
                    tabLogin.setTypeface(Constants.SFTEXT_BOLD);
                    tabSignup.setTypeface(Constants.SFTEXT_REGULAR);
                    rlLogin.setVisibility(View.VISIBLE);
                    rlSignup.setVisibility(View.GONE);
                } else if (position == SIGNUP) {
                    isFromLogin = false;
                    tabLogin.setTypeface(Constants.SFTEXT_REGULAR);
                    tabSignup.setTypeface(Constants.SFTEXT_BOLD);
                    rlLogin.setVisibility(View.GONE);
                    rlSignup.setVisibility(View.VISIBLE);
                }
            }
        });

        segmentLoginGroup.setPosition(isFromLogin ? 0 : 1);

        ccp.registerCarrierNumberEditText(etPhone);
    }

    private void initFacebook() {
        LoginManager.getInstance().logOut();
        defaultFbLogin.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        hideProgress();
                    }

                    @Override
                    public void onError(FacebookException e) {
                        if (e instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                        Log.e("LoginActivity", e.getMessage());
                        hideProgress();
                    }
                });
    }

    public String getEmail() {
        return isFromLogin ? etEmail.getText().toString() : etSEmail.getText().toString();
    }

    public String getPassword() {
        return isFromLogin ? etPassword.getText().toString() : etSPassword.getText().toString();
    }

    public String getPhone() {
        return etPhone.getText().toString();
    }

    public String getCountryName() {
        return ccp.getSelectedCountryName().toLowerCase();
    }

    public String getCountryISOCode() {
        return ccp.getSelectedCountryNameCode().toLowerCase();
    }

    public String getCountryCode() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public void login() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().login(getEmail(), getPassword(), getToken(), getDeviceType(),
                getTimeZone(), getToken());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("Login Response", json);
                if (checkStatus(json)) {
                    UserModel userModel = new Gson().fromJson(json, UserModel.class);
                    if (userModel != null) {
                        if (checkStatus(userModel)) {
                            if (userModel.data != null) {
                                afterLoginOrSignup(userModel.data);
                            }
                        } else {
                            failureError(userModel.msg);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("login failed");
            }
        });
    }

    public void register() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().registerStep1(getEmail(), getPassword(), getCountryCode(), getPhone(), getToken());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("CreateStep1 Response", json);
                if (checkStatus(json)) {
                    UserModel userModel = new Gson().fromJson(json, UserModel.class);
                    if (userModel != null) {
                        if (checkStatus(userModel)) {
                            if (userModel.data != null) {
                                setupData(userModel.data);
                                redirectActivity(NameActivity.class);
                                finish();
                            }
                        } else {
                            failureError(userModel.msg);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    public void socialLogin(String socialId, String first_name, String last_name, String email) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().socialLogin(email, first_name, last_name, "", "",
                "", getTimeZone(), "", "", getToken(), socialId);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    String json = new Gson().toJson(response.body());
                    Log.e("Login Response", json);
                    if (checkStatus(json)) {
                        UserModel userModel = new Gson().fromJson(json, UserModel.class);
                        if (userModel != null) {
                            if (checkStatus(userModel)) {
                                if (userModel.data != null) {
                                    afterLoginOrSignup(userModel.data);
                                }
                            } else {
                                failureError(userModel.msg);
                            }
                        } else {
                            failureError("No data found");
                        }
                    }
                    LoginManager.getInstance().logOut();
                    googleSignOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("social login failed");
            }
        });
    }

    private void setupData(UserModel.Data data) {
        Preferences.writeBoolean(LoginSignupNewActivity.this, Constants.IS_LOGIN, true);
        Preferences.saveUserData(LoginSignupNewActivity.this, data);

        Intercom.client().setUserHash(generateHMACKey("5R6iP59lkPAbdkg2VVSNnPVJI--HKQn6malYWpq0", data.userId));
        Registration registration = Registration.create();
        registration.withEmail(data.email);
        registration.withUserId(data.userId);
        Intercom.client().registerIdentifiedUser(registration);

        WoopraTracker tracker = Woopra.getInstance(this).getTracker("cheapestessay.com");
        tracker.setVisitorProperty("name", data.firstName);
        tracker.setVisitorProperty("email", data.email);
        tracker.push();
    }

    public void afterLoginOrSignup(UserModel.Data data) {
        setupData(data);
        finish();
    }

    private String generateHMACKey(String secret, String message) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hash = (sha256_HMAC.doFinal(message.getBytes()));
            StringBuffer result = new StringBuffer();
            for (byte b : hash) {
                result.append(String.format("%02x", b));
            }
            Log.e("HMAC Key", result.toString());
            return result.toString();
        }
        catch (Exception e){
            System.out.println("Error");
        }
        return "";
    }

    @OnClick({R.id.img_back, R.id.btn_login, R.id.tv_forgot_password, R.id.fb_login, R.id.google_login, R.id.twitter_login,
            R.id.btn_signup, R.id.fb_signup, R.id.google_signup, R.id.twitter_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_login:
                if (validLoginData()) {
                    login();
                }
                break;
            case R.id.btn_signup:
                if (validSignUpData()) {
                    register();
                }
                break;
            case R.id.tv_forgot_password:
                showForgotPasswordDialog();
                break;
            case R.id.fb_signup:
            case R.id.fb_login:
                showProgress();
                //LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.google_signup:
            case R.id.google_login:
                showProgress();
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.twitter_login:
            case R.id.twitter_signup:
//                showProgress();
//                defaultTwitterLogin.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
//                    @Override
//                    public void success(Result<TwitterSession> result) {
//                        Log.e("Twitter Success", result.data.getUserName());
//                        hideProgress();
//                    }
//
//                    @Override
//                    public void failure(TwitterException exception) {
//                        Log.e("twitterLogin:failure", exception.getMessage());
//                        hideProgress();
//                    }
//                });
                break;
        }
    }

    public boolean validLoginData() {
        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public boolean validSignUpData() {
        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (!ccp.isValidFullNumber()) {
            validationError("Enter Valid Mobile no");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etEmail = dialog.findViewById(R.id.et_email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(etEmail.getText().toString())) {
                    Utils.hideSoftKeyboard(LoginSignupNewActivity.this);
                    forgotPassword(etEmail.getText().toString(), false);
                    dialog.dismiss();
                } else {
                    toastMessage("Enter valid email");
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void forgotPassword(final String email, final boolean isResend) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().forgotPassword(email);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        if (!isResend)
                            showSecurityCodeDialog(email);
                    }
                    toastMessage(response.body().msg);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("forgot password failed");
            }
        });
    }

    public void showSecurityCodeDialog(final String email) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_security_code);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etSecurityCode = dialog.findViewById(R.id.et_security_code);
        final EditText etNewPassword = dialog.findViewById(R.id.et_new_password);
        TextView tvResendCode = dialog.findViewById(R.id.tv_resend_code);
        tvResendCode.setPaintFlags(tvResendCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword(email, true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(etSecurityCode.getText().toString())) {
                    validationError("Please enter code");
                    return;
                }

                if (isEmpty(etNewPassword.getText().toString())) {
                    validationError("Please enter password");
                    return;
                }

                Utils.hideSoftKeyboard(LoginSignupNewActivity.this);
                resetPassword(etSecurityCode.getText().toString(), etNewPassword.getText().toString(), dialog);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void resetPassword(String otp, String password, final Dialog dialog) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().resetPassword(getAccessToken(), password, otp);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        toastMessage(response.body().msg);
                        dialog.dismiss();
                    } else {
                        failureError(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        defaultTwitterLogin.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if (account != null) {
                    firebaseAuthWithGoogle(account);
                }
            } catch (ApiException e) {
                hideProgress();
                Log.e("Google fails", e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null)
                                Log.e("FirebaseGoogle Login", user.getDisplayName());
                        } else {
                            Log.e("FirebaseGoogle Fails", task.getException().getMessage());
                        }
                        String json = new Gson().toJson(acct);
                        Log.e("Google Response", json);
                        hideProgress();
                        socialLogin(acct.getId(), acct.getGivenName(), acct.getFamilyName(), acct.getEmail());
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null)
                                Log.e("FirebaseFB Login", user.getDisplayName());
                        } else {
                            Log.e("FirebaseFB Fails", task.getException().getMessage());
                        }

                        GraphRequest request = GraphRequest.newMeRequest(
                                token,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        hideProgress();
                                        try {
                                            String id = object.getString("id");
                                            String first_name = "", last_name = "", email = "";
                                            if (object.has("first_name")) {
                                                first_name = object.getString("first_name");
                                            }
                                            if (object.has("last_name")) {
                                                last_name = object.getString("last_name");
                                            }
                                            if (object.has("email")) {
                                                email = object.getString("email");
                                            }
                                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                                            Log.e("Fb Response", object.toString());

                                            socialLogin(id, first_name, last_name, email);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }
                });
    }

    private void googleSignOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Login Signup Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open login signup screen");
        setTracker(event);
    }
}
