package com.cheapestessay.service.ui.profile;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cheapestessay.service.R;
import com.cheapestessay.service.adapter.RecyclerviewAdapter;
import com.cheapestessay.service.model.Balance;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.EqualSpacingItemDecoration;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.util.textview.TextViewSFTextBold;
import com.cheapestessay.service.woopra.WoopraEvent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalanceActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.rv_balance)
    RecyclerView rvBalance;
    @BindView(R.id.tv_balance)
    TextViewSFTextBold tvBalance;

    private List<Balance.Data> balanceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        ButterKnife.bind(this);

        balanceList = new ArrayList<>();

        tvToolbarTitle.setText(getString(R.string.balance).toUpperCase());
        tvRight.setText(getString(R.string.chat));

        rvBalance.setLayoutManager(new LinearLayoutManager(this));
        rvBalance.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        rvBalance.setFocusable(false);

        tvBalance.setText(Utils.priceWith$(Preferences.getUserData(this).balance));

        getBalanceHistory();
    }

    @OnClick({R.id.ll_back, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                openIntercomChat();
                break;
        }
    }

    public void getBalanceHistory() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Balance> call = getService().getBalanceHistory(getUserId());
        call.enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(Call<Balance> call, Response<Balance> response) {
                Balance model = response.body();
                if (model != null) {
                    if (checkStatus(model)) {
                        balanceList = model.data;
                        RecyclerviewAdapter mAdapter = new RecyclerviewAdapter((ArrayList<?>) balanceList,
                                R.layout.item_balance_history, BalanceActivity.this);
                        rvBalance.setAdapter(mAdapter);
                    } else {
                        toastMessage(model.msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Balance> call, Throwable t) {
                failureError("get balance history failed");
            }
        });
    }

    @Override
    public void bindView(View view, int position) {
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvCustomer = view.findViewById(R.id.tv_customer);
        TextView tvDate = view.findViewById(R.id.tv_date_time);
        TextView tvAmount = view.findViewById(R.id.tv_amount);
        TextView tvBalance = view.findViewById(R.id.tv_balance);

        Balance.Data balance = balanceList.get(position);

        tvTitle.setText(balance.journalText);
        tvCustomer.setText(balance.transactiontext);
        tvDate.setText(balance.date);
        if (balance.transactionamount.contains("+")) {
            tvAmount.setBackground(ContextCompat.getDrawable(this, R.drawable.green_bg_button));
        } else {
            tvAmount.setBackground(ContextCompat.getDrawable(this, R.drawable.red_button_bg));
        }
        tvAmount.setText("Amount: " + balance.transactionamount);
        tvBalance.setText("Balance: " + balance.currentWalletBalance);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Balance History Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open balance history screen");
        setTracker(event);
    }
}
