package com.cheapestessay.service.ui.profile;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.textview.TextViewSFTextBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LifeTimeDiscountActivity extends BaseActivity {

    @BindView(R.id.tv_how_it_works)
    TextViewSFTextRegular tvHowItWorks;
    @BindView(R.id.tv_from_amount)
    TextViewSFTextBold tvFromAmount;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_to_amount)
    TextViewSFTextBold tvToAmount;
    @BindView(R.id.tv_away_member)
    TextViewSFTextBold tvAwayMember;
    @BindView(R.id.tv_here_blue)
    TextViewSFTextBold tvHereBlue;
    @BindView(R.id.tv_here_silver)
    TextViewSFTextBold tvHereSilver;
    @BindView(R.id.tv_here_gold)
    TextViewSFTextBold tvHereGold;
    @BindView(R.id.tv_here_vip)
    TextViewSFTextBold tvHereVip;
    @BindView(R.id.img_medal_blue)
    ImageView imgMedalBlue;
    @BindView(R.id.ll_medal_blue)
    LinearLayout llMedalBlue;
    @BindView(R.id.img_medal_silver)
    ImageView imgMedalSilver;
    @BindView(R.id.ll_medal_silver)
    LinearLayout llMedalSilver;
    @BindView(R.id.img_medal_gold)
    ImageView imgMedalGold;
    @BindView(R.id.ll_medal_gold)
    LinearLayout llMedalGold;
    @BindView(R.id.img_medal_vip)
    ImageView imgMedalVip;
    @BindView(R.id.ll_medal_vip)
    LinearLayout llMedalVip;

    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_time_discount);
        ButterKnife.bind(this);

        tvHowItWorks.setPaintFlags(tvHowItWorks.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        userData = Preferences.getUserData(this);

        imgMedalBlue.setImageResource(R.drawable.medal_blue);
        imgMedalSilver.setImageResource(R.drawable.medal_silver);
        imgMedalGold.setImageResource(R.drawable.medal_gold);
        imgMedalVip.setImageResource(R.drawable.medal_vip);

        getBalance();
    }

    public void getBalance() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserModel> call = getService().getBalance(getUserId());
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel model = response.body();
                if (checkStatus(model)) {
                    userData.balance = model.data.balance;
                    userData.discountName = model.data.discountName;
                    userData.benifitValue = model.data.benifitValue;
                    userData.totalPayment = model.data.totalPayment;
                    Preferences.saveUserData(LifeTimeDiscountActivity.this, userData);
                    double fromNum = Double.parseDouble(userData.totalPayment);
                    if (!isEmpty(model.data.discountName)) {
                        if (model.data.discountName.equalsIgnoreCase("+Bronze") ||
                                model.data.discountName.equalsIgnoreCase("+Blue")) {
                            setProgressUi(fromNum, 100, "Silver", tvHereBlue, llMedalBlue);
                            imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                        } else if (model.data.discountName.equalsIgnoreCase("+Silver")) {
                            imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                            imgMedalSilver.setImageResource(R.drawable.medal_silver_fill);
                            setProgressUi(fromNum, 300, "Gold", tvHereSilver, llMedalSilver);
                        } else if (model.data.discountName.equalsIgnoreCase("+Gold")) {
                            imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                            imgMedalSilver.setImageResource(R.drawable.medal_silver_fill);
                            imgMedalGold.setImageResource(R.drawable.medal_gold_fill);
                            setProgressUi(fromNum, 1500, "VIP", tvHereGold, llMedalGold);
                        } else if (model.data.discountName.equalsIgnoreCase("+VIP")) {
                            imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                            imgMedalSilver.setImageResource(R.drawable.medal_silver_fill);
                            imgMedalGold.setImageResource(R.drawable.medal_gold_fill);
                            imgMedalVip.setImageResource(R.drawable.medal_vip_fill);
                            progress.setProgress(100);
                            setProgressUi(0, 0, "", tvHereVip, llMedalVip);
                        } else {
                            setProgressUi(fromNum, 100, "Silver", tvHereBlue, llMedalBlue);
                            imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                        }
                    } else {
                        setProgressUi(fromNum, 100, "Silver", tvHereBlue, llMedalBlue);
                        imgMedalBlue.setImageResource(R.drawable.medal_blue_fill);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("get balance failed");
            }
        });
    }

    private void setProgressUi(double fromNumber, int toNumber, String medalName, TextViewSFTextBold tvHereText, LinearLayout llMedalView) {
        if (tvHereText != null)
            tvHereText.setVisibility(View.VISIBLE);
        if (llMedalView != null)
            llMedalView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.shadow_bg));
        if (toNumber == 0 && fromNumber == 0) {
            tvAwayMember.setText(getString(R.string.you_are_vip_member));
        } else {
            double totalPayment = Double.parseDouble(Utils.priceWithout$(userData.totalPayment));
            double awayAmount = toNumber - totalPayment;
            String away = Utils.priceWith$(String.valueOf(awayAmount));
            tvAwayMember.setText(getString(R.string.away_member, away, medalName));

            double totalDiff = toNumber - fromNumber;
            //double totalSpend = totalPayment - fromNumber;
            int percentage = (int) ((fromNumber * 100) / totalDiff);
            progress.setProgress(percentage);
        }
        tvToAmount.setText(Utils.priceWith$(String.valueOf(toNumber)));
        tvFromAmount.setText(Utils.priceWith$(String.valueOf(fromNumber)));
    }

    @OnClick({R.id.ll_back, R.id.tv_how_it_works})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_how_it_works:
                Intent i = new Intent(this, LifeTimeHowItWorkActivity.class);
                i.putExtra(Constants.MEDAL_LEVEL, userData.discountName);
                startActivity(i);
                openToLeft();
//                if (isEmpty(getAccessToken())) {
//                    Preferences.saveUserData(this, null);
//                    goToLoginSignup(true);
//                } else {
//                    redirectUsingCustomTab(getString(R.string.autologin_url, Constants.DISCOUNT, getAccessToken()));
//                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Life Time Discount Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open life time discount screen");
        setTracker(event);
    }
}
