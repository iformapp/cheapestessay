package com.cheapestessay.service.ui.home;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.adapter.DeadlineAdapter;
import com.cheapestessay.service.adapter.TypesAdapter;
import com.cheapestessay.service.model.Deadline;
import com.cheapestessay.service.model.PriceCalculate;
import com.cheapestessay.service.model.TypesModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.ui.neworder.NewOrderActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.WrapContentHeightViewPager;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.Woopra;
import com.cheapestessay.service.woopra.WoopraEvent;
import com.cheapestessay.service.woopra.WoopraTracker;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.btn_college)
    TextViewSFTextRegular btnCollege;
    @BindView(R.id.btn_bachelor)
    TextViewSFTextRegular btnBachelor;
    @BindView(R.id.btn_master)
    TextViewSFTextRegular btnMaster;
    @BindView(R.id.tv_3hours)
    TextViewSFTextRegular tv3hours;
    @BindView(R.id.tv_6hours)
    TextViewSFTextRegular tv6hours;
    @BindView(R.id.tv_12hours)
    TextViewSFTextRegular tv12hours;
    @BindView(R.id.tv_24hours)
    TextViewSFTextRegular tv24hours;
    @BindView(R.id.tv_2days)
    TextViewSFTextRegular tv2days;
    @BindView(R.id.tv_4days)
    TextViewSFTextRegular tv4days;
    @BindView(R.id.tv_10days)
    TextViewSFTextRegular tv10days;
    @BindView(R.id.tv_7days)
    TextViewSFTextRegular tv7days;
    @BindView(R.id.tv_14days)
    TextViewSFTextRegular tv14days;
    @BindView(R.id.tv_pages)
    TextViewSFTextRegular tvPages;
    @BindView(R.id.viewpager)
    WrapContentHeightViewPager viewpager;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.tv_paper_type)
    TextViewSFTextRegular tvPaperType;
    @BindView(R.id.tv_price)
    TextViewSFDisplayBold tvPrice;
    @BindView(R.id.tv_deadline_date)
    TextViewSFTextRegular tvDeadlineDate;
    @BindView(R.id.rv_deadlines)
    RecyclerView rvDeadlines;

    private ArrayList<HomePagerModel> arrayList;
    private TypesAdapter typesAdapter;
    private String deadlineType, deadlineValue;
    private int writerLevelId = Constants.COLLEGE_ID;
    private String paperTypeId = "1";
    private DeadlineAdapter deadlineAdapter;
    private ArrayList<Deadline> deadlineArrayList = new ArrayList<>();
    private Timer timer;
    private int DELAY_TIME = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        //initDeadlineViews();
        deadlineArrayList = getDeadlinesArray();
        rvDeadlines.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        deadlineAdapter = new DeadlineAdapter(this, deadlineArrayList, true);
        rvDeadlines.setAdapter(deadlineAdapter);

        List<TypesModel.Data> paperData = Preferences.getTypes(this, Constants.PAPER_TYPES);
        if (paperData != null && paperData.size() > 0) {
            for (int i = 0; i < paperData.size(); i++) {
                if (paperData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                    paperTypeId = paperData.get(i).paperId;
                    break;
                }
            }
        }

        int lastPosition = deadlineArrayList.size() - 1;
        deadlineAdapter.doRefresh(lastPosition);
        onDeadlineSelect(deadlineArrayList.get(lastPosition));

        fillPagerData();

        HomePagerAdapter adapter = new HomePagerAdapter(this, arrayList);
        viewpager.setAdapter(adapter);
        indicator.setViewPager(viewpager);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(12);
    }

    public void fillPagerData() {
        arrayList = new ArrayList<>();
        HomePagerModel model = new HomePagerModel();
        model.title = getString(R.string.guarantee_text);
        model.icon = R.drawable.guarantee;
        model.details = getString(R.string.read_more);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.offer_text);
        model.icon = R.drawable.offer;
        model.details = getString(R.string.offer_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.plagiarism_text);
        model.icon = R.drawable.plagiarism;
        model.details = getString(R.string.plagiarism_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.cheapest_text);
        model.icon = R.drawable.cheapest;
        model.details = getString(R.string.cheapest_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.data_text);
        model.icon = R.drawable.data;
        model.details = getString(R.string.read_more);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.support_text);
        model.icon = R.drawable.support;
        model.details = getString(R.string.chat_now);
        arrayList.add(model);
    }

    @OnClick({R.id.btn_college, R.id.btn_bachelor, R.id.btn_master, R.id.tv_paper_type, R.id.tv_3hours, R.id.tv_6hours,
            R.id.tv_12hours, R.id.tv_24hours, R.id.tv_2days, R.id.tv_4days, R.id.tv_10days, R.id.tv_7days, R.id.tv_14days,
            R.id.img_minus, R.id.img_plus, R.id.img_next, R.id.img_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_college:
                writerLevelId = Constants.COLLEGE_ID;
                unselectWriterAll();
                btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                btnCollege.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_bachelor:
                writerLevelId = Constants.BACHELOR_ID;
                unselectWriterAll();
                btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                btnBachelor.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
            case R.id.btn_master:
                writerLevelId = Constants.MASTER_ID;
                unselectWriterAll();
                btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                btnMaster.setTextColor(ContextCompat.getColor(this, R.color.white));
                getPrice();
                break;
//            case R.id.tv_3hours:
//                deadlineType = getString(R.string.hours);
//                deadlineValue = "3";
//                unselectDeadlineAll();
//                selectedView(tv3hours, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_6hours:
//                deadlineType = getString(R.string.hours);
//                deadlineValue = "6";
//                unselectDeadlineAll();
//                selectedView(tv6hours, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_12hours:
//                deadlineType = getString(R.string.hours);
//                deadlineValue = "12";
//                unselectDeadlineAll();
//                selectedView(tv12hours, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_24hours:
//                deadlineType = getString(R.string.hours);
//                deadlineValue = "24";
//                unselectDeadlineAll();
//                selectedView(tv24hours, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_2days:
//                deadlineType = getString(R.string.days);
//                deadlineValue = "2";
//                unselectDeadlineAll();
//                selectedView(tv2days, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_4days:
//                deadlineType = getString(R.string.days);
//                deadlineValue = "4";
//                unselectDeadlineAll();
//                selectedView(tv4days, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_10days:
//                deadlineType = getString(R.string.days);
//                deadlineValue = "10";
//                unselectDeadlineAll();
//                selectedView(tv10days, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_7days:
//                deadlineType = getString(R.string.days);
//                deadlineValue = "7";
//                unselectDeadlineAll();
//                selectedView(tv7days, deadlineValue, deadlineType);
//                break;
//            case R.id.tv_14days:
//                deadlineType = getString(R.string.days);
//                deadlineValue = "14";
//                unselectDeadlineAll();
//                selectedView(tv14days, deadlineValue, deadlineType);
//                break;
            case R.id.img_minus:
                int page = Integer.parseInt(tvPages.getText().toString());
                if (page != 1) {
                    tvPages.setText(String.valueOf(page - 1));
                    if (timer != null) {
                        timer.cancel();
                    }

                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getPrice();
                        }
                    }, DELAY_TIME);
                }
                break;
            case R.id.img_plus:
                page = Integer.parseInt(tvPages.getText().toString());
                tvPages.setText(String.valueOf(page + 1));

                if (timer != null) {
                    timer.cancel();
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getPrice();
                    }
                }, DELAY_TIME);
                break;
            case R.id.img_next:
                Preferences.writeString(this, Constants.ORDER_ID, "");
                Map<String, String> map = new HashMap<>();
                map.put(Constants.DEADLINE_TYPE, deadlineType);
                map.put(Constants.DEADLINE_VALUE, deadlineValue);
                map.put(Constants.PAGE, getPage());
                map.put(Constants.WRITER_LEVEL_ID, String.valueOf(writerLevelId));
                map.put(Constants.PAPER_TYPES, tvPaperType.getText().toString());
                Intent i = new Intent(this, NewOrderActivity.class);
                i.putExtra(Constants.ORDER_DATA, (Serializable) map);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
                break;
            case R.id.img_more:
                scrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.smoothScrollTo(0, indicator.getBottom());
                    }
                }, 100);
                break;
            case R.id.tv_paper_type:
                showItemSelectDialog(Constants.PAPER_TYPES);
                break;
        }
    }

    public String getPage() {
        return tvPages.getText().toString();
    }

    public void getPrice() {
        if (!isNetworkConnected())
            return;

        Map<String, String> map = new HashMap<>();
        map.put("serviceTypeId", "3");
        map.put("paperTypeId", paperTypeId);
        map.put("paperTypeOther", "");
        map.put("subjectId", "5");
        map.put("subjectOther", "");
        map.put("chart", "0");
        map.put("source", "0");
        map.put("slide", "0");
        map.put("formatStyleId", "2");
        map.put("formatStyleOther", "");
        map.put("disciplineId", "3");
        map.put("writer", "any_writer");
        map.put("topic", "");
        map.put("spacing", "1");
        map.put("orderDetail", "");
        map.put("deadlineType", deadlineType);
        map.put("deadlineValue", deadlineValue);
        map.put("writerLevelId", String.valueOf(writerLevelId));
        map.put("page", getPage());
        map.put("is_send_to_my_email", "0");
        map.put("is_abstruct_page", "0");
        map.put("is_plagiarism_report", "0");
        map.put("writerId", "");
        map.put("user_id", getUserId());
        map.put("order_id", "");

        Log.e("Get Price Url = > ", Constants.BASE_URL + Constants.PRICE_CALCULATE);
        Log.e("Price Params", map.toString());

        Call<Object> call = getService().getPrice(map);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("Price response", json);
                PriceCalculate getPrice = new Gson().fromJson(json, PriceCalculate.class);
                if (getPrice != null) {
                    if (checkStatus(getPrice)) {
                        tvPrice.setText("$" + Utils.numberFormat2Places(getPrice.data.total));
                        tvDeadlineDate.setText(getPrice.data.deadline);
                    } else {
                        failureError(getPrice.msg);
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("get price failed");
            }
        });
    }

    public void unselectWriterAll() {
        btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        blackTextView(btnCollege, btnBachelor, btnMaster);
    }

    public void unselectDeadlineAll() {
        tv3hours.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        tv14days.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        whiteBackgroundView(tv6hours, tv12hours, tv24hours, tv2days, tv4days, tv7days, tv10days);
        initDeadlineViews();
    }

    public void initDeadlineViews() {
        setTextAndColor(tv3hours, "3", getString(R.string.hours));
        setTextAndColor(tv6hours, "6", getString(R.string.hours));
        setTextAndColor(tv12hours, "12", getString(R.string.hours));
        setTextAndColor(tv24hours, "24", getString(R.string.hours));
        setTextAndColor(tv2days, "2", getString(R.string.days));
        setTextAndColor(tv4days, "4", getString(R.string.days));
        setTextAndColor(tv7days, "7", getString(R.string.days));
        setTextAndColor(tv10days, "10", getString(R.string.days));
        setTextAndColor(tv14days, "14", getString(R.string.days));
    }

    public ArrayList<Deadline> getDeadlinesArray() {
        ArrayList<Deadline> deadlineArrayList = new ArrayList<>();
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "3"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "6"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "12"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "24"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "48"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "3"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "4"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "5"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "7"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "10"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "14"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "19"));
        return deadlineArrayList;
    }

    public void setTextAndColor(TextView textView, String text, String time) {
        String htmltext;
        if (time.equals("Hours")) {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#D0555A'><small>" + time + "</small></font>";
        } else {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#13B675'><small>" + time + "</small></font>";
        }

        textView.setText(Utils.fromHtml(htmltext));
    }

    public void selectedView(TextView textView, String text, String time) {
        switch (text) {
            case "3":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                break;
            case "14":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                break;
            default:
                textView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
        }
        String htmltext = "<font color='#ffffff'><big>" + text + "</big></font><br><font color='#ffffff'><small>" + time + "</small></font>";
        textView.setText(Utils.fromHtml(htmltext));

        getPrice();
    }

    public void showItemSelectDialog(final String types) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.type_of_paper)));

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        List<TypesModel.Data> mData = Preferences.getTypes(this, types);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                    mData.get(i).isSelected = true;
                }
            }
            typesAdapter = new TypesAdapter(this, mData, types);
            rvTypes.setAdapter(typesAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typesAdapter != null && typesAdapter.getSelectedItem() != null) {
                    if (types.equals(Constants.PAPER_TYPES)) {
                        paperTypeId = typesAdapter.getSelectedItem().paperId;
                        tvPaperType.setText(typesAdapter.getSelectedItem().paperName);
                    }
                }
                getPrice();
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (typesAdapter != null)
                    typesAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(HomeActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    public void onDeadlineSelect(Deadline item) {
        deadlineType = item.deadlineType;
        deadlineValue = item.deadlineValue;
        getPrice();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Home Screen");
        event.setProperty(Constants.EVENT_TITLE, "User Open Home Screen");
        setTracker(event);
    }
}
