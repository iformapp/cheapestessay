package com.cheapestessay.service.ui.neworder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.adapter.DeadlineAdapter;
import com.cheapestessay.service.adapter.RecyclerviewAdapter;
import com.cheapestessay.service.adapter.TypesAdapter;
import com.cheapestessay.service.adapter.UploadFileAdapter;
import com.cheapestessay.service.model.CouponCode;
import com.cheapestessay.service.model.Deadline;
import com.cheapestessay.service.model.FileUpload;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.model.PriceCalculate;
import com.cheapestessay.service.model.SaveOrder;
import com.cheapestessay.service.model.TypesModel;
import com.cheapestessay.service.model.UserFiles;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.util.textview.TextViewSFTextBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.activity.VideoPickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.vincent.filepicker.filter.entity.VideoFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class NewOrderActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.btn_writing)
    TextViewSFTextRegular btnWriting;
    @BindView(R.id.btn_editing)
    TextViewSFTextRegular btnEditing;
    @BindView(R.id.btn_powerpoint)
    TextViewSFTextRegular btnPowerpoint;
    @BindView(R.id.btn_college)
    TextViewSFTextRegular btnCollege;
    @BindView(R.id.btn_bachelor)
    TextViewSFTextRegular btnBachelor;
    @BindView(R.id.btn_master)
    TextViewSFTextRegular btnMaster;
    @BindView(R.id.tv_paper_type)
    TextViewSFTextRegular tvPaperType;
    @BindView(R.id.tv_subject)
    TextViewSFTextRegular tvSubject;
//    @BindView(R.id.tv_3hours)
//    TextViewSFTextRegular tv3hours;
//    @BindView(R.id.tv_6hours)
//    TextViewSFTextRegular tv6hours;
//    @BindView(R.id.tv_12hours)
//    TextViewSFTextRegular tv12hours;
//    @BindView(R.id.tv_24hours)
//    TextViewSFTextRegular tv24hours;
//    @BindView(R.id.tv_2days)
//    TextViewSFTextRegular tv2days;
//    @BindView(R.id.tv_4days)
//    TextViewSFTextRegular tv4days;
//    @BindView(R.id.tv_7days)
//    TextViewSFTextRegular tv7days;
//    @BindView(R.id.tv_10days)
//    TextViewSFTextRegular tv10days;
//    @BindView(R.id.tv_15days)
//    TextViewSFTextRegular tv15days;
    @BindView(R.id.tv_words)
    TextViewSFTextRegular tvWords;
    @BindView(R.id.tv_pages)
    TextViewSFTextRegular tvPages;
    @BindView(R.id.et_topic)
    EditTextSFTextRegular etTopic;
    @BindView(R.id.et_details)
    EditTextSFTextRegular etDetails;
    @BindView(R.id.tv_format_style)
    TextViewSFDisplayRegular tvFormatStyle;
    @BindView(R.id.tv_discipline)
    TextViewSFDisplayRegular tvDiscipline;
    @BindView(R.id.tv_preferred_writer)
    TextViewSFDisplayRegular tvPreferredWriter;
    @BindView(R.id.img_abstract_page)
    ImageView imgAbstractPage;
    @BindView(R.id.img_turnitin)
    ImageView imgTurnitin;
    @BindView(R.id.img_send_email)
    ImageView imgSendEmail;
    @BindView(R.id.et_couponcode)
    EditTextSFTextRegular etCouponcode;
    @BindView(R.id.ll_coupon_code)
    LinearLayout llCouponCode;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.tv_toolbar_price)
    TextViewSFTextBold tvToolbarPrice;
    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.ll_more)
    LinearLayout llMore;
    @BindView(R.id.tv_have_code)
    TextViewSFTextRegular tvHaveCode;
    @BindView(R.id.tv_coupon_code)
    TextViewSFTextRegular tvCouponCode;
    @BindView(R.id.rl_applying_code)
    RelativeLayout rlApplyingCode;
    @BindView(R.id.img_arrow)
    ImageView imgArrow;
    @BindView(R.id.tv_spaced_page)
    TextViewSFTextRegular tvSpacedPage;
    @BindView(R.id.et_other_paper)
    EditTextSFTextRegular etOtherPaper;
    @BindView(R.id.et_other_subject)
    EditTextSFTextRegular etOtherSubject;
    @BindView(R.id.tv_files_count)
    TextViewSFTextRegular tvFilesCount;
    @BindView(R.id.et_other_format)
    EditTextSFTextRegular etOtherFormat;
    @BindView(R.id.et_other_writer)
    EditTextSFTextRegular etOtherWriter;
    @BindView(R.id.tv_category)
    TextViewSFDisplayRegular tvCategory;
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @BindView(R.id.tv_no_files)
    TextViewSFDisplayBold tvNoFiles;
    @BindView(R.id.tv_total)
    TextViewSFDisplayRegular tvTotal;
    @BindView(R.id.root)
    LinearLayout root;
    @BindView(R.id.tv_more_options)
    TextViewSFTextRegular tvMoreOptions;
    @BindView(R.id.tv_old_total)
    TextViewSFDisplayRegular tvOldTotal;
    @BindView(R.id.tv_deadline_date)
    TextViewSFTextRegular tvDeadlineDate;
    @BindView(R.id.ll_files)
    LinearLayout llFiles;
    @BindView(R.id.img_minus)
    ImageView imgMinus;
    @BindView(R.id.img_plus)
    ImageView imgPlus;
    @BindView(R.id.rl_page_count)
    RelativeLayout rlPageCount;
    @BindView(R.id.tv_title)
    TextViewSFTextBold tvTitle;
    @BindView(R.id.ll_show_option)
    LinearLayout llShowOption;
    @BindView(R.id.rv_deadlines)
    RecyclerView rvDeadlines;

    private RecyclerviewAdapter mAdapter;
    private UploadFileAdapter fileAdapter;
    private ArrayList<HashMap<String, String>> pageTypeArray;
    private boolean isAbstractPage = false;
    private boolean isTurnitinReport = false;
    private boolean isSendEmail = false;
    private TypesAdapter typesAdapter;
    private String deadlineType, deadlineValue;
    private int writerLevelId = Constants.COLLEGE_ID;
    private int serviceTypeId = Constants.WRITING_ID;
    private String paperTypeId = "1";
    private String prefferedId = "any_writer";
    private String subjectId = "1";
    private String disciplineId = "1";
    private String formatStyleId = "10";
    private String spacing = "1"; // 1 for double and 2 for single space
    private String chartCount = "0";
    private String sourceCount = "0";
    private String slideCount = "0";
    private Map<String, String> map;
    private Timer timer;
    private ArrayList<File> fileList;
    private OrderByIdModel.Data orderData;
    private boolean isRefresh = false;
    private boolean isEditOrder = false;
    private DeadlineAdapter deadlineAdapter;
    private ArrayList<Deadline> deadlineArrayList = new ArrayList<>();
    private int DELAY_TIME = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        ButterKnife.bind(this);

        init();
    }

    public void init() {
        //initDeadlineViews();
        deadlineArrayList = getDeadlinesArray();
        rvDeadlines.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        deadlineAdapter = new DeadlineAdapter(this, deadlineArrayList, false);
        rvDeadlines.setAdapter(deadlineAdapter);

        tvOldTotal.setPaintFlags(tvOldTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        rvOrders.setLayoutManager(new LinearLayoutManager(this));
        rvFiles.setLayoutManager(new LinearLayoutManager(this));

        pageTypeArray = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(getString(R.string.sorces), "0");
        pageTypeArray.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put(getString(R.string.charts), "0");
        pageTypeArray.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put(getString(R.string.powerpoint_slide), "0");
        pageTypeArray.add(hashMap);
        mAdapter = new RecyclerviewAdapter(pageTypeArray, R.layout.item_new_order_types, this);
        rvOrders.setAdapter(mAdapter);

        tvCouponCode.setPaintFlags(tvCouponCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        etDetails.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etDetails.setRawInputType(InputType.TYPE_CLASS_TEXT);
        editTextOnTextChangeEvent(etOtherPaper, etOtherSubject, etTopic, etDetails);

        isEditOrder = !isEmpty(Preferences.readString(this, Constants.EDIT_ORDER_ID, ""));
        tvTitle.setText(isEditOrder ? getString(R.string.edit_order) : getString(R.string.new_order));

        if (!isEmpty(getOrderId())) {
            getOrderById(getOrderId(), false);
        } else {
            setAllTypesIds();
            if (getIntent() != null) {
                map = (Map<String, String>) getIntent().getSerializableExtra(Constants.ORDER_DATA);
                if (map != null) {
                    writerLevelId = Integer.parseInt(map.get(Constants.WRITER_LEVEL_ID));
                    deadlineType = map.get(Constants.DEADLINE_TYPE);
                    deadlineValue = map.get(Constants.DEADLINE_VALUE);
                    tvPages.setText(map.get(Constants.PAGE));
                    tvPaperType.setText(map.get(Constants.PAPER_TYPES));
                    if (map.get(Constants.PAPER_TYPES).toLowerCase().contains("other")) {
                        etOtherPaper.setVisibility(View.VISIBLE);
                    }
                    List<TypesModel.Data> paperData = Preferences.getTypes(this, Constants.PAPER_TYPES);
                    if (paperData != null && paperData.size() > 0) {
                        for (int i = 0; i < paperData.size(); i++) {
                            if (paperData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                                paperTypeId = paperData.get(i).paperId;
                                break;
                            }
                        }
                    }

                    for (int i = 0; i < deadlineArrayList.size(); i++) {
                        if (deadlineArrayList.get(i).deadlineValue.equals(deadlineValue)) {
                            deadlineAdapter.doRefresh(i);
                            break;
                        }
                    }

                    switch (writerLevelId) {
                        case Constants.COLLEGE_ID:
                            btnCollege.performClick();
                            break;
                        case Constants.MASTER_ID:
                            btnMaster.performClick();
                            break;
                        case Constants.BACHELOR_ID:
                            btnBachelor.performClick();
                            break;
                    }
                } else {
                    int lastPosition = deadlineArrayList.size() - 1;
                    deadlineAdapter.doRefresh(lastPosition);
                    onDeadlineSelect(deadlineArrayList.get(lastPosition));
                }
            } else {
                int lastPosition = deadlineArrayList.size() - 1;
                deadlineAdapter.doRefresh(lastPosition);
                onDeadlineSelect(deadlineArrayList.get(lastPosition));
            }
        }
    }

    private void setOrderData(OrderByIdModel orderModel) {
        orderData = orderModel.data;
        setService(orderData.service);
        setWriter(orderData.academic);
        tvPaperType.setText(orderData.paperName);
        if (orderData.paperName.toLowerCase().contains("other")) {
            etOtherPaper.setVisibility(View.VISIBLE);
            etOtherPaper.setText(orderData.otherPaperName);
        }
        tvSubject.setText(orderData.subjectName);
        if (orderData.subjectName.toLowerCase().contains("other")) {
            etOtherSubject.setVisibility(View.VISIBLE);
            etOtherSubject.setText(orderData.otherSubjectName);
        }
        deadlineType = orderData.deadlineType;
        deadlineValue = orderData.deadlineValue;
        for (int i = 0; i < deadlineArrayList.size(); i++) {
            if (deadlineArrayList.get(i).deadlineValue.equals(deadlineValue)) {
                deadlineAdapter.doRefresh(i);
                break;
            }
        }
        tvPages.setText(orderData.page);
        if (orderData.spacing.contains("Single")) {
            spacing = "2";
            tvWords.setText("560 words");
        } else {
            spacing = "1";
            tvWords.setText("280 words");
        }
        tvSpacedPage.setText(orderData.spacing.trim());
        etTopic.setText(orderData.topic);
        etDetails.setText(Utils.fromHtml(orderData.additionalDetail));
        List<UserFiles> userFiles = orderData.userFiles;
        List<FileUpload.Data> filesData = new ArrayList<>();
        if (userFiles != null && userFiles.size() > 0) {
            for (int i = 0; i < userFiles.size(); i++) {
                FileUpload.Data fileUpload = new FileUpload.Data();
                fileUpload.fileId = userFiles.get(i).fileId;
                fileUpload.filename = userFiles.get(i).filename;
                fileUpload.filepath = userFiles.get(i).filepath;
                fileUpload.orderId = userFiles.get(i).orderId;
                fileUpload.hashedFileName = userFiles.get(i).hashedFileName;
                fileUpload.fileUploadSimpleDate = userFiles.get(i).fileUploadSimpleDate;
                filesData.add(fileUpload);
            }
            setFileAdapter(filesData);
        }
        tvFormatStyle.setText(orderData.styleName);
        tvDiscipline.setText(orderData.discipline);
        tvPreferredWriter.setText(orderData.preferredWriter);
        setAllTypesIds();
        if (orderData.plagiarismReport.equalsIgnoreCase("Yes")) {
            imgTurnitin.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            isTurnitinReport = true;
        }
        if (orderData.abstructPage.equalsIgnoreCase("Yes")) {
            imgAbstractPage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            isAbstractPage = true;
        }
        if (orderData.isSendToMyEmail.equalsIgnoreCase("1")) {
            imgSendEmail.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            isSendEmail = true;
        }
        sourceCount = orderData.source;
        chartCount = orderData.charts;
        slideCount = orderData.powerpoint;
        pageTypeArray = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(getString(R.string.sorces), sourceCount);
        pageTypeArray.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put(getString(R.string.charts), chartCount);
        pageTypeArray.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put(getString(R.string.powerpoint_slide), slideCount);
        pageTypeArray.add(hashMap);
        mAdapter.notifyDataSetChanged();
//        if (!isEmpty(orderData.couponCode)) {
//            llCouponCode.setVisibility(View.GONE);
//            rlApplyingCode.setVisibility(View.VISIBLE);
//            tvOldTotal.setVisibility(View.VISIBLE);
//            tvCouponCode.setText(getCouponCode());
//        }
        getPrice();
    }

    private void setAllTypesIds() {
        List<TypesModel.Data> paperData = Preferences.getTypes(this, Constants.PAPER_TYPES);
        if (paperData != null && paperData.size() > 0) {
            for (int i = 0; i < paperData.size(); i++) {
                if (paperData.get(i).paperName.equalsIgnoreCase(tvPaperType.getText().toString())) {
                    paperTypeId = paperData.get(i).paperId;
                    break;
                }
            }
        }

        List<TypesModel.Data> disciplinData = Preferences.getTypes(this, Constants.DISCIPLIN_TYPES);
        if (disciplinData != null && disciplinData.size() > 0) {
            for (int i = 0; i < disciplinData.size(); i++) {
                if (disciplinData.get(i).discipline.equalsIgnoreCase(tvDiscipline.getText().toString())) {
                    disciplineId = disciplinData.get(i).disciplineId;
                    break;
                }
            }
        }

        List<TypesModel.Data> formatData = Preferences.getTypes(this, Constants.FORMATED_STYLE_TYPES);
        if (formatData != null && formatData.size() > 0) {
            for (int i = 0; i < formatData.size(); i++) {
                if (formatData.get(i).styleName.equalsIgnoreCase(tvFormatStyle.getText().toString())) {
                    formatStyleId = formatData.get(i).styleId;
                    break;
                }
            }
        }

        List<TypesModel.Data> subjectData = Preferences.getTypes(this, Constants.SUBJECTS_TYPES);
        if (subjectData != null && subjectData.size() > 0) {
            for (int i = 0; i < subjectData.size(); i++) {
                if (subjectData.get(i).subjectName.equalsIgnoreCase(tvSubject.getText().toString())) {
                    subjectId = subjectData.get(i).id;
                    break;
                }
            }
        }

        List<TypesModel.Data> writerData = Preferences.getTypes(this, Constants.ACADEMIC_TYPES);
        if (writerData != null && writerData.size() > 0) {
            for (int i = 0; i < writerData.size(); i++) {
                if (writerData.get(i).academicLevelName.equalsIgnoreCase(tvPreferredWriter.getText().toString())) {
                    prefferedId = writerData.get(i).academicLevelId;
                    break;
                }
            }
        }
    }

    private void editTextOnTextChangeEvent(EditText... editTexts) {
        for (EditText editText : editTexts) {
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // nothing to do here
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getPrice();
                        }
                    }, 600);
                }
            });
        }
    }

    public String getPage() {
        return tvPages.getText().toString();
    }

    public void getPrice() {
        if (!isNetworkConnected())
            return;

        //setPageType();

        Map<String, String> map = new HashMap<>();
        map.put("serviceTypeId", String.valueOf(serviceTypeId));
        map.put("paperTypeId", paperTypeId);
        map.put("paperTypeOther", getOtherPaperName());
        map.put("subjectId", subjectId);
        map.put("subjectOther", getOtherSubject());
        map.put("chart", chartCount);
        map.put("source", sourceCount);
        map.put("slide", slideCount);
        map.put("formatStyleId", formatStyleId);
        map.put("formatStyleOther", getOtherFormat());
        map.put("disciplineId", disciplineId);
        map.put("writer", prefferedId);
        map.put("topic", etTopic.getText().toString());
        map.put("spacing", spacing);
        map.put("orderDetail", Html.toHtml(etDetails.getText()));
        map.put("deadlineType", deadlineType);
        map.put("deadlineValue", deadlineValue);
        map.put("writerLevelId", String.valueOf(writerLevelId));
        map.put("page", getPage());
        map.put("is_send_to_my_email", isSendEmail ? "7" : "0");
        map.put("is_abstruct_page", isAbstractPage ? "6" : "0");
        map.put("is_plagiarism_report", isTurnitinReport ? "5" : "0");
        map.put("writerId", getOldWriter());
        map.put("user_id", getUserId());
        map.put("order_id", getOrderId());

        Log.e("Get Price Url = > ", Constants.BASE_URL + (isEditOrder ? Constants.PRICE_CALCULATE_EDIT : Constants.PRICE_CALCULATE));
        Log.e("Price Params", map.toString());

        Call<Object> call;
        if (isEditOrder)
            call = getService().getPriceForEditOrder(map);
        else
            call = getService().getPrice(map);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("Price response", json);
                PriceCalculate model = new Gson().fromJson(json, PriceCalculate.class);
                if (model != null) {
                    if (checkStatus(model)) {
                        if (model.data.orderId != null)
                            Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, model.data.orderId);

                        tvToolbarPrice.setText("$" + Utils.numberFormat2Places(model.data.total));
                        tvTotal.setText("$" + Utils.numberFormat2Places(model.data.total));
                        tvOldTotal.setText("$" + Utils.numberFormat2Places(model.data.subTotal));
                        tvDeadlineDate.setText(model.data.deadline);
                    } else {
                        failureError(model.msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("get price failed");
            }
        });
    }

    public void updateOrder() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Map<String, String> map = new HashMap<>();
        map.put("serviceTypeId", String.valueOf(serviceTypeId));
        map.put("paperTypeId", paperTypeId);
        map.put("paperTypeOther", getOtherPaperName());
        map.put("subjectId", subjectId);
        map.put("subjectOther", getOtherSubject());
        map.put("chart", chartCount);
        map.put("source", sourceCount);
        map.put("slide", slideCount);
        map.put("formatStyleId", formatStyleId);
        map.put("formatStyleOther", getOtherFormat());
        map.put("disciplineId", disciplineId);
        map.put("writer", prefferedId);
        map.put("topic", etTopic.getText().toString());
        map.put("spacing", spacing);
        map.put("orderDetail", Html.toHtml(etDetails.getText()));
        map.put("deadlineType", deadlineType);
        map.put("deadlineValue", deadlineValue);
        map.put("writerLevelId", String.valueOf(writerLevelId));
        map.put("page", getPage());
        map.put("is_send_to_my_email", isSendEmail ? "7" : "0");
        map.put("is_abstruct_page", isAbstractPage ? "6" : "0");
        map.put("is_plagiarism_report", isTurnitinReport ? "5" : "0");
        map.put("writerId", getOldWriter());
        map.put("user_id", getUserId());
        map.put("order_id", getOrderId());
        map.put("accesstoken", getAccessToken());

        Log.e("Update Order Url = > ", Constants.BASE_URL + Constants.UPDATE_ORDER);
        Log.e("Update Order Params", map.toString());

        Call<Object> call = getService().updateOrder(map);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("Update Order response", json);
                PriceCalculate model = new Gson().fromJson(json, PriceCalculate.class);
                if (model != null) {
                    if (checkStatus(model)) {
                        finish();
                        finishToBottom();
                    } else {
                        failureError(model.msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("update order failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        final TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvPrice = view.findViewById(R.id.tv_price);
        final TextView tvPages = view.findViewById(R.id.tv_pages);
        final ImageView imgMinus = view.findViewById(R.id.img_minus);
        ImageView imgPlus = view.findViewById(R.id.img_plus);

        tvTitle.setText(pageTypeArray.get(position).keySet().toArray()[0].toString());
        tvPages.setText(pageTypeArray.get(position).get(tvTitle.getText().toString()));
        if (position == 0) {
            tvPrice.setText("( FREE )");
        } else {
            tvPrice.setText("");
        }

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    showToolTip(v, getString(R.string.tooltip_sources));
                } else if (position == 1) {
                    showToolTip(v, getString(R.string.tooltip_charts));
                } else {
                    showToolTip(v, getString(R.string.tooltip_powerpoint));
                }
            }
        });

        imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int page = Integer.parseInt(tvPages.getText().toString());
                if (page != 0) {
                    tvPages.setText(String.valueOf(page - 1));
                    pageTypeArray.get(position).put(tvTitle.getText().toString(), tvPages.getText().toString());
                }
                if (position == 0) {
                    sourceCount = tvPages.getText().toString();
                } else if (position == 1) {
                    chartCount = tvPages.getText().toString();
                } else {
                    slideCount = tvPages.getText().toString();
                }
                getPrice();
                if (tvPages.getText().toString().equals("0")) {
                    imgMinus.setEnabled(false);
                }
            }
        });

        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgMinus.setEnabled(true);
                int page = Integer.parseInt(tvPages.getText().toString());
                tvPages.setText(String.valueOf(page + 1));
                pageTypeArray.get(position).put(tvTitle.getText().toString(), tvPages.getText().toString());
                if (position == 0) {
                    sourceCount = tvPages.getText().toString();
                } else if (position == 1) {
                    chartCount = tvPages.getText().toString();
                } else {
                    slideCount = tvPages.getText().toString();
                }
                getPrice();
            }
        });
    }

    @OnClick({R.id.tv_back, R.id.tv_question, R.id.tv_type_service_info, R.id.btn_writing, R.id.btn_editing, R.id.btn_powerpoint,
            R.id.tv_writer_info, R.id.btn_college, R.id.btn_bachelor, R.id.btn_master, R.id.tv_type_paper_info, R.id.tv_subject_info,
            R.id.tv_pages_info, R.id.img_minus, R.id.img_plus, R.id.tv_topic_info, R.id.tv_details_info,
            R.id.tv_upload_info, R.id.rl_upload_files, R.id.rl_abstract_page, R.id.rl_turnitin, R.id.rl_email, R.id.ll_more,
            R.id.tv_apply, R.id.img_coupon_close, R.id.rl_save_order, R.id.tv_have_code, R.id.ll_show_option, R.id.tv_coupon_remove,
            R.id.tv_paper_type, R.id.tv_subject, R.id.tv_format_style, R.id.tv_discipline, R.id.tv_preferred_writer, R.id.rl_page_type,
            R.id.tv_category, R.id.tv_format_info, R.id.tv_discipline_info, R.id.tv_preferred_writer_info, R.id.tv_abstract_info,
            R.id.tv_turnitin_info, R.id.tv_email_info, R.id.tv_deadline_info})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(this);
        switch (view.getId()) {
            case R.id.tv_back:
                onBackPressed();
                break;
            case R.id.tv_question:
                openIntercomChat();
                break;
            case R.id.tv_type_service_info:
                showToolTip(view, getString(R.string.tooltip_service));
                break;
            case R.id.btn_writing:
                setService(getString(R.string.writing));
                getPrice();
                break;
            case R.id.btn_editing:
                tvPaperType.setText("Editing");
                setAllTypesIds();
                setService(getString(R.string.editing));
                getPrice();
                break;
            case R.id.btn_powerpoint:
                tvPaperType.setText(getString(R.string.powerpoint_presentation));
                setAllTypesIds();
                setService(getString(R.string.powerpoint));
                getPrice();
                break;
            case R.id.tv_writer_info:
                showToolTip(view, getString(R.string.tooltip_writer));
                break;
            case R.id.btn_college:
                setWriter(getString(R.string.college));
                getPrice();
                break;
            case R.id.btn_bachelor:
                setWriter(getString(R.string.bachelor));
                getPrice();
                break;
            case R.id.btn_master:
                setWriter(getString(R.string.master));
                getPrice();
                break;
            case R.id.tv_type_paper_info:
                showToolTip(view, getString(R.string.tooltip_paper));
                break;
            case R.id.tv_subject_info:
                showToolTip(view, getString(R.string.tooltip_subject));
                break;
//            case R.id.tv_3hours:
//                setDeadline("3", getString(R.string.hours));
//                break;
//            case R.id.tv_6hours:
//                setDeadline("6", getString(R.string.hours));
//                break;
//            case R.id.tv_12hours:
//                setDeadline("12", getString(R.string.hours));
//                break;
//            case R.id.tv_24hours:
//                setDeadline("24", getString(R.string.hours));
//                break;
//            case R.id.tv_2days:
//                setDeadline("2", getString(R.string.days));
//                break;
//            case R.id.tv_4days:
//                setDeadline("4", getString(R.string.days));
//                break;
//            case R.id.tv_10days:
//                setDeadline("10", getString(R.string.days));
//                break;
//            case R.id.tv_7days:
//                setDeadline("7", getString(R.string.days));
//                break;
//            case R.id.tv_15days:
//                setDeadline("15", getString(R.string.days));
//                break;
            case R.id.tv_pages_info:
                showToolTip(view, getString(R.string.tooltip_pages));
                break;
            case R.id.img_minus:
                int page = Integer.parseInt(tvPages.getText().toString());
                if (page != 1) {
                    tvPages.setText(String.valueOf(page - 1));
                    if (timer != null) {
                        timer.cancel();
                    }

                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getPrice();
                        }
                    }, DELAY_TIME);
                }
                break;
            case R.id.img_plus:
                page = Integer.parseInt(tvPages.getText().toString());
                tvPages.setText(String.valueOf(page + 1));
                if (timer != null) {
                    timer.cancel();
                }

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getPrice();
                    }
                }, DELAY_TIME);
                break;
            case R.id.rl_page_type:
                Dialog dialog = onCreateDialogSingleChoice();
                dialog.show();
                break;
            case R.id.tv_topic_info:
                showToolTip(view, getString(R.string.tooltip_topic));
                break;
            case R.id.tv_details_info:
                showToolTip(view, getString(R.string.tooltip_details));
                break;
            case R.id.tv_upload_info:
                showToolTip(view, getString(R.string.tooltip_upload));
                break;
            case R.id.tv_format_info:
                showToolTip(view, getString(R.string.tooltip_format));
                break;
            case R.id.tv_discipline_info:
                showToolTip(view, getString(R.string.tooltip_discipline));
                break;
            case R.id.tv_preferred_writer_info:
                showToolTip(view, getString(R.string.tooltip_preferred_writer));
                break;
            case R.id.tv_abstract_info:
                showToolTip(view, getString(R.string.tooltip_abstract));
                break;
            case R.id.tv_turnitin_info:
                showToolTip(view, getString(R.string.tooltip_turnitin));
                break;
            case R.id.tv_email_info:
                showToolTip(view, getString(R.string.tooltip_send_it_mail));
                break;
            case R.id.tv_deadline_info:
                showToolTip(view, getString(R.string.tooltip_deadline));
                break;
            case R.id.rl_upload_files:
                selectFileDialog();
                break;
            case R.id.rl_abstract_page:
                imgAbstractPage.setImageDrawable(ContextCompat.getDrawable(this,
                        isAbstractPage ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isAbstractPage = !isAbstractPage;
                getPrice();
                break;
            case R.id.rl_turnitin:
                imgTurnitin.setImageDrawable(ContextCompat.getDrawable(this,
                        isTurnitinReport ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isTurnitinReport = !isTurnitinReport;
                getPrice();
                break;
            case R.id.rl_email:
                imgSendEmail.setImageDrawable(ContextCompat.getDrawable(this,
                        isSendEmail ? R.drawable.radio_unselect : R.drawable.radio_selected));
                isSendEmail = !isSendEmail;
                getPrice();
                break;
            case R.id.tv_apply:
                if (isEmpty(getCouponCode())) {
                    toastMessage("enter coupon code");
                    return;
                }
                applyCouponCode(true);
                break;
            case R.id.img_coupon_close:
                tvHaveCode.setVisibility(View.VISIBLE);
                llCouponCode.setVisibility(View.GONE);
                etCouponcode.setText("");
                break;
            case R.id.tv_coupon_remove:
                applyCouponCode(false);
                break;
            case R.id.rl_save_order:
                if (isLogin()) {
                    if (isValid()) {
                        if (isEditOrder) {
                            updateOrder();
                        } else {
                            saveOrder();
                        }
                    }
                } else {
                    goToLoginSignup(true);
                }
                break;
            case R.id.tv_have_code:
                tvHaveCode.setVisibility(View.GONE);
                llCouponCode.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_show_option:
                if (llMore.getVisibility() == View.GONE) {
                    llMore.setVisibility(View.VISIBLE);
                    imgArrow.setRotation(180);
                    tvMoreOptions.setText(getString(R.string.hide_more_options));
                } else {
                    llMore.setVisibility(View.GONE);
                    imgArrow.setRotation(0);
                    tvMoreOptions.setText(getString(R.string.show_more_options));
                }
                break;
            case R.id.tv_paper_type:
                showItemSelectDialog(Constants.PAPER_TYPES);
                break;
            case R.id.tv_subject:
                showItemSelectDialog(Constants.SUBJECTS_TYPES);
                break;
            case R.id.tv_format_style:
                showItemSelectDialog(Constants.FORMATED_STYLE_TYPES);
                break;
            case R.id.tv_discipline:
                showItemSelectDialog(Constants.DISCIPLIN_TYPES);
                break;
            case R.id.tv_preferred_writer:
                showItemSelectDialog(Constants.ACADEMIC_TYPES);
                break;
            case R.id.tv_category:
                showItemSelectDialog(Constants.CATEGORY_TYPES);
                break;
        }
    }

    private void setDeadline(String value, String type) {
        deadlineType = type;
        deadlineValue = value;
        selectedView(deadlineValue, deadlineType);
        getPrice();
    }

    private void setService(String serviceType) {
        unselectServiceTypeAll();

        if (serviceType.equals(getString(R.string.writing))) {
            rlPageCount.setAlpha(1f);
            imgMinus.setEnabled(true);
            imgPlus.setEnabled(true);
            tvPages.setText("1");
            slideCount = "0";
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(getString(R.string.powerpoint_slide), "0");
            pageTypeArray.set(2, hashMap);
            mAdapter.notifyDataSetChanged();
            serviceTypeId = Constants.WRITING_ID;
            btnWriting.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
            btnWriting.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else if (serviceType.equals(getString(R.string.editing))) {
            rlPageCount.setAlpha(1f);
            imgMinus.setEnabled(true);
            imgPlus.setEnabled(true);
            tvPages.setText("1");
            slideCount = "0";
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(getString(R.string.powerpoint_slide), "0");
            pageTypeArray.set(2, hashMap);
            mAdapter.notifyDataSetChanged();
            serviceTypeId = Constants.EDITING_ID;
            btnEditing.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnEditing.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else if (serviceType.equals(getString(R.string.powerpoint))) {
            if (llMore.getVisibility() == View.GONE) {
                llMore.setVisibility(View.VISIBLE);
                imgArrow.setRotation(180);
                tvMoreOptions.setText(getString(R.string.hide_more_options));
            }

            rlPageCount.setAlpha(0.5f);
            imgMinus.setEnabled(false);
            imgPlus.setEnabled(false);
            tvPages.setText("0");
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(getString(R.string.powerpoint_slide), "1");
            pageTypeArray.set(2, hashMap);
            mAdapter.notifyDataSetChanged();
            slideCount = "1";
            serviceTypeId = Constants.POWERPOINT_ID;
            btnPowerpoint.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
            btnPowerpoint.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void setWriter(String writerType) {
        unselectWriterAll();
        if (writerType.equals(getString(R.string.college))) {
            writerLevelId = Constants.COLLEGE_ID;
            btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
            btnCollege.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else if (writerType.equals(getString(R.string.bachelor))) {
            writerLevelId = Constants.BACHELOR_ID;
            btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnBachelor.setTextColor(ContextCompat.getColor(this, R.color.white));
        } else if (writerType.equals(getString(R.string.master))) {
            writerLevelId = Constants.MASTER_ID;
            btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
            btnMaster.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    public String getOtherSubject() {
        return etOtherSubject.getText().toString().trim();
    }

    public String getOtherPaperName() {
        return etOtherPaper.getText().toString().trim();
    }

    public String getOtherFormat() {
        return etOtherFormat.getText().toString().trim();
    }

    public String getOldWriter() {
        return etOtherWriter.getText().toString().trim();
    }

    public String getTopic() {
        return etTopic.getText().toString().trim();
    }

    public String getPrefferedWriter() {
        return tvPreferredWriter.getText().toString().trim();
    }

    public String getDetails() {
        return etDetails.getText().toString().trim();
    }

    public boolean isValid() {
        if (getPaperName().toLowerCase().contains("other") && isEmpty(getOtherPaperName())) {
            validationError("Please provide the other Type of your Paper");
            return false;
        }

        if (getSubject().toLowerCase().contains("other") && isEmpty(getOtherSubject())) {
            validationError("Please provide the Other Subject");
            return false;
        }

        if (isEmpty(getTopic())) {
            validationError("Please enter topic");
            return false;
        }

        if (isEmpty(getDetails())) {
            validationError("Please enter atleast 3 words in Details");
            return false;
        }

        String[] split = getDetails().split(" ");
        if (split.length < 3) {
            validationError("Please enter atleast 3 words in Details");
            return false;
        }

        if (prefferedId.equals("my_previous_writer") && isEmpty(getOldWriter())) {
            validationError("Please enter old writer");
            return false;
        }

        if (serviceTypeId == Constants.POWERPOINT_ID && slideCount.equals("0")) {
            validationError("PowerPoint slides is required");
            return false;
        }

        return true;
    }

    public String getCouponCode() {
        return etCouponcode.getText().toString().trim();
    }

    public void applyCouponCode(final boolean isApply) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<CouponCode> call = getService().applyCode(isApply ? getCouponCode() : "x", getAccessToken(), getOrderId(), getUserId());
        call.enqueue(new Callback<CouponCode>() {
            @Override
            public void onResponse(Call<CouponCode> call, Response<CouponCode> response) {
                CouponCode model = response.body();
                if (model != null) {
                    if (checkStatus(model)) {
                        llCouponCode.setVisibility(View.GONE);
                        rlApplyingCode.setVisibility(View.VISIBLE);
                        tvOldTotal.setVisibility(View.VISIBLE);
                        tvCouponCode.setText(getCouponCode());
                        tvOldTotal.setText("$" + Utils.numberFormat2Places(model.data.subTotal));
                        tvToolbarPrice.setText("$" + Utils.numberFormat2Places(model.data.total));
                        tvTotal.setText("$" + Utils.numberFormat2Places(model.data.total));
                    } else {
                        if (model.data != null) {
                            etCouponcode.setText("");
                            llCouponCode.setVisibility(View.VISIBLE);
                            rlApplyingCode.setVisibility(View.GONE);
                            tvOldTotal.setVisibility(View.GONE);
                            tvToolbarPrice.setText("$" + Utils.numberFormat2Places(model.data.total));
                            tvTotal.setText("$" + Utils.numberFormat2Places(model.data.total));
                            if (!isApply) {
                                toastMessage("Coupon code successfully removed.");
                            } else {
                                failureError(model.msg);
                            }
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CouponCode> call, Throwable t) {
                failureError("get code failed");
            }
        });
    }

    public void saveOrder() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Log.e("SaveOrder Url = > ", Constants.BASE_URL + Constants.SET_ORDER_STATUS);
        Log.e("Params => ", "accesstoken : " + getAccessToken() + " " + getOrderId());

        Call<Object> call = getService().saveOrder(getAccessToken(), getOrderId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("SaveOrder Response : ", json);
                SaveOrder model = new Gson().fromJson(new Gson().toJson(response.body()), SaveOrder.class);
                if (checkStatus(model)) {
                    Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, "");
                    getOrderById(model.data.orderId, true);
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("save order failed");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "New Order Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open new order screen");
        setTracker(event);
        if (isLogin() && isRefresh) {
            isRefresh = false;
            getOrderById(getOrderId(), false);
        }
    }

    public void getOrderById(String orderId, final boolean isCheckOut) {
        if (!isNetworkConnected()) {
            return;
        }

        if (!isProgressShowing())
            showProgress();

        Log.e("GetOrdersById Url = > ", Constants.BASE_URL + Constants.ORDER_DETAIL);
        Log.e("Params => ", "accesstoken : " + getAccessToken() + ", user_id : " + getUserId() + ", order_id : " + orderId);

        Call<OrderByIdModel> call = getService().getOrderById(getAccessToken(), getUserId(), orderId);
        call.enqueue(new Callback<OrderByIdModel>() {
            @Override
            public void onResponse(Call<OrderByIdModel> call, Response<OrderByIdModel> response) {
                String json = new Gson().toJson(response.body());
                Log.e("GetOrdersById Response ", json);
                OrderByIdModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    if (isCheckOut) {
                        toastMessage("Order has been saved");
                        Intent i = new Intent(NewOrderActivity.this, CheckoutActivity.class);
                        i.putExtra(Constants.ORDER_DATA, orderModel.data);
                        startActivity(i);
                        finish();
                        openToTop();
                    } else {
                        setOrderData(orderModel);
                    }
                } else {
                    isRefresh = true;
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<OrderByIdModel> call, Throwable t) {
                failureError(getString(R.string.order_details_not_found));
            }
        });
    }

    public void selectFileDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_camera_document_select);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        LinearLayout llCamera = dialog.findViewById(R.id.ll_camera);
        LinearLayout llVideo = dialog.findViewById(R.id.ll_video);
        LinearLayout llDocument = dialog.findViewById(R.id.ll_document);

        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false, false);
                dialog.dismiss();
            }
        });

        llVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false, true);
                dialog.dismiss();
            }
        });

        llDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(true, false);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void checkPermission(final boolean isDocument, final boolean isVideo) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isDocument) {
                                Intent intent = new Intent(NewOrderActivity.this, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else if (isVideo) {
                                Intent intent = new Intent(NewOrderActivity.this, VideoPickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 2);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_VIDEO);
                            } else {
                                Intent intent = new Intent(NewOrderActivity.this, ImagePickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileList = new ArrayList<>();
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        for (NormalFile file : docPaths) {
                            fileList.add(new File(file.getPath()));
                        }
                        uploadFile();
                    } else {
                        toastMessage("File not selected");
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        for (ImageFile file : imgPath) {
                            fileList.add(new File(file.getPath()));
                        }
                        uploadFile();
                    } else {
                        toastMessage("File not selected");
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_VIDEO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<VideoFile> videoPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_VIDEO);
                    if (videoPath != null && videoPath.size() > 0) {
                        for (VideoFile file : videoPath) {
                            fileList.add(new File(file.getPath()));
                        }
                        uploadFile();
                    } else {
                        toastMessage("File not selected");
                    }
                }
                break;
        }
    }

    public void uploadFile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part[] body = null;
        if (fileList != null && fileList.size() > 0) {
            body = new MultipartBody.Part[fileList.size()];
            for (int i = 0; i < fileList.size(); i++) {
                File file = fileList.get(i);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

                RequestBody requestFile = null;
                if (mimeType != null) {
                    requestFile = RequestBody.create(MediaType.parse(mimeType), file);
                }

                if (requestFile != null) {
                    body[i] = MultipartBody.Part.createFormData("qqfile[]", file.getName(), requestFile);
                }
            }
        }

        RequestBody order_id = RequestBody.create(MultipartBody.FORM, getOrderId());
        RequestBody user_id = RequestBody.create(MultipartBody.FORM, getUserId());
        RequestBody accesstoken = RequestBody.create(MultipartBody.FORM, getAccessToken());
        RequestBody category = RequestBody.create(MultipartBody.FORM, "");

        Log.e("Upload file Url = > ", Constants.BASE_URL + Constants.FILE_UPLOAD);

        Call<FileUpload> call = getService().uploadMaterial(body, category, order_id, user_id, accesstoken);
        call.enqueue(new Callback<FileUpload>() {
            @Override
            public void onResponse(Call<FileUpload> call, Response<FileUpload> response) {
                FileUpload fileUpload = response.body();
                if (checkStatus(fileUpload)) {
                    setFileAdapter(fileUpload.data);
                }

                hideProgress();
            }

            @Override
            public void onFailure(Call<FileUpload> call, Throwable t) {
                failureError("file upload failed");
            }
        });
    }

    public void setFileAdapter(List<FileUpload.Data> uploadedfiles) {
        if (uploadedfiles != null && uploadedfiles.size() > 0) {
            tvFilesCount.setText(uploadedfiles.size() + " Files");

            rvFiles.setVisibility(View.VISIBLE);
            tvNoFiles.setVisibility(View.GONE);

            if (fileAdapter == null) {
                fileAdapter = new UploadFileAdapter(NewOrderActivity.this);
            }

            fileAdapter.doRefresh(uploadedfiles);

            if (rvFiles.getAdapter() == null) {
                rvFiles.setAdapter(fileAdapter);
            }
            llFiles.setVisibility(View.VISIBLE);
        } else {
            llFiles.setVisibility(View.GONE);
            tvFilesCount.setText("0 Files");
            rvFiles.setVisibility(View.GONE);
            tvNoFiles.setVisibility(View.VISIBLE);
        }
    }

    public void showItemSelectDialog(final String types) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        switch (types) {
            case Constants.PAPER_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.type_of_paper)));
                break;
            case Constants.ACADEMIC_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.preferred_writer)));
                break;
            case Constants.SUBJECTS_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.subject)));
                break;
            case Constants.DISCIPLIN_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.discipline)));
                break;
            case Constants.FORMATED_STYLE_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.format_style)));
                break;
            case Constants.CATEGORY_TYPES:
                etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.category)));
                break;
        }

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        List<TypesModel.Data> mData = Preferences.getTypes(this, types);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                switch (types) {
                    case Constants.PAPER_TYPES:
                        if (mData.get(i).paperName.equalsIgnoreCase(getPaperName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.ACADEMIC_TYPES:
                        if (mData.get(i).academicLevelName.equalsIgnoreCase(getPrefferedWriter())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.SUBJECTS_TYPES:
                        if (mData.get(i).subjectName.equalsIgnoreCase(getSubject())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.DISCIPLIN_TYPES:
                        if (mData.get(i).discipline.equalsIgnoreCase(getDisciplinName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.FORMATED_STYLE_TYPES:
                        if (mData.get(i).styleName.equalsIgnoreCase(getFormatName())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                    case Constants.CATEGORY_TYPES:
                        if (mData.get(i).category.equalsIgnoreCase(getCategory())) {
                            mData.get(i).isSelected = true;
                        }
                        break;
                }
            }
            typesAdapter = new TypesAdapter(this, mData, types);
            rvTypes.setAdapter(typesAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(NewOrderActivity.this);
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(NewOrderActivity.this);
                if (typesAdapter != null && typesAdapter.getSelectedItem() != null) {
                    switch (types) {
                        case Constants.PAPER_TYPES:
                            paperTypeId = typesAdapter.getSelectedItem().paperId;
                            tvPaperType.setText(typesAdapter.getSelectedItem().paperName);
                            if (getPaperName().toLowerCase().contains("other")) {
                                etOtherPaper.setVisibility(View.VISIBLE);
                            } else {
                                etOtherPaper.setText("");
                                etOtherPaper.setVisibility(View.GONE);
                                if (getPaperName().equals(getString(R.string.editing))) {
                                    btnEditing.performClick();
                                    break;
                                } else if (getPaperName().equals(getString(R.string.powerpoint_presentation))) {
                                    btnPowerpoint.performClick();
                                    break;
                                }
                            }
                            getPrice();
                            break;
                        case Constants.ACADEMIC_TYPES:
                            prefferedId = typesAdapter.getSelectedItem().academicLevelId;
                            tvPreferredWriter.setText(typesAdapter.getSelectedItem().academicLevelName);
                            if (tvPreferredWriter.getText().toString().equalsIgnoreCase(getString(R.string.my_old_writer))) {
                                etOtherWriter.setVisibility(View.VISIBLE);
                            } else {
                                etOtherWriter.setText("");
                                etOtherWriter.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.SUBJECTS_TYPES:
                            subjectId = typesAdapter.getSelectedItem().id;
                            tvSubject.setText(typesAdapter.getSelectedItem().subjectName);
                            if (getSubject().toLowerCase().contains("other")) {
                                etOtherSubject.setVisibility(View.VISIBLE);
                            } else {
                                etOtherSubject.setText("");
                                etOtherSubject.setVisibility(View.GONE);
                            }
                            getPrice();
                            break;
                        case Constants.DISCIPLIN_TYPES:
                            disciplineId = typesAdapter.getSelectedItem().disciplineId;
                            tvDiscipline.setText(typesAdapter.getSelectedItem().discipline);
                            getPrice();
                            break;
                        case Constants.FORMATED_STYLE_TYPES:
                            formatStyleId = typesAdapter.getSelectedItem().styleId;
                            tvFormatStyle.setText(typesAdapter.getSelectedItem().styleName);
//                            if (getFormatName().toLowerCase().contains("other")) {
//                                etOtherFormat.setVisibility(View.VISIBLE);
//                            } else {
//                                etOtherFormat.setText("");
//                                etOtherFormat.setVisibility(View.GONE);
//                            }
                            getPrice();
                            break;
                        case Constants.CATEGORY_TYPES:
                            tvCategory.setText(typesAdapter.getSelectedItem().category);
                            break;
                    }
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (typesAdapter != null) {
                    typesAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(NewOrderActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    public String getPaperName() {
        return tvPaperType.getText().toString();
    }

    public String getSubject() {
        return tvSubject.getText().toString();
    }

    public String getDisciplinName() {
        return tvDiscipline.getText().toString();
    }

    public String getFormatName() {
        return tvFormatStyle.getText().toString();
    }

    public String getCategory() {
        return tvCategory.getText().toString();
    }

    public void unselectServiceTypeAll() {
        btnWriting.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        btnPowerpoint.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        btnEditing.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        blackTextView(btnWriting, btnEditing, btnPowerpoint);
    }

    public void unselectWriterAll() {
        btnCollege.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
        btnMaster.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
        btnBachelor.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        blackTextView(btnCollege, btnBachelor, btnMaster);
    }

//    public void unselectDeadlineAll() {
//        tv3hours.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_unselect));
//        tv15days.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_unselect));
//        whiteBackgroundView(tv6hours, tv12hours, tv24hours, tv2days, tv4days, tv7days, tv10days);
//        initDeadlineViews();
//    }
//
//    public void initDeadlineViews() {
//        setTextAndColor(tv3hours, "3", getString(R.string.hours));
//        setTextAndColor(tv6hours, "6", getString(R.string.hours));
//        setTextAndColor(tv12hours, "12", getString(R.string.hours));
//        setTextAndColor(tv24hours, "24", getString(R.string.hours));
//        setTextAndColor(tv2days, "2", getString(R.string.days));
//        setTextAndColor(tv4days, "4", getString(R.string.days));
//        setTextAndColor(tv7days, "7", getString(R.string.days));
//        setTextAndColor(tv10days, "10", getString(R.string.days));
//        setTextAndColor(tv15days, "15", getString(R.string.days));
//    }

    public ArrayList<Deadline> getDeadlinesArray() {
        ArrayList<Deadline> deadlineArrayList = new ArrayList<>();
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "3"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "6"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "12"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "24"));
        deadlineArrayList.add(new Deadline(getString(R.string.hours), "48"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "3"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "4"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "5"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "7"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "10"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "14"));
        deadlineArrayList.add(new Deadline(getString(R.string.days), "19"));
        return deadlineArrayList;
    }

    public void setTextAndColor(TextView textView, String text, String time) {
        String htmltext;
        if (time.equals("Hours")) {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#D0555A'><small>" + time + "</small></font>";
        } else {
            htmltext = "<font color=black><big>" + text + "</big></font><br><font color='#13B675'><small>" + time + "</small></font>";
        }

        textView.setText(Utils.fromHtml(htmltext));
    }

    public void selectedView(String text, String time) {
        int resID = getResources().getIdentifier("tv_" + text.toLowerCase() + time.toLowerCase(),
                "id", getPackageName());
        TextView textView = findViewById(resID);
        switch (text) {
            case "3":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.left_corner_select));
                break;
            case "15":
                textView.setBackground(ContextCompat.getDrawable(this, R.drawable.right_corner_select));
                break;
            default:
                textView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
        }
        String htmltext = "<font color='#ffffff'><big>" + text + "</big></font><br><font color='#ffffff'><small>" + time + "</small></font>";
        textView.setText(Utils.fromHtml(htmltext));
    }

    public void onDeadlineSelect(Deadline item) {
        deadlineType = item.deadlineType;
        deadlineValue = item.deadlineValue;
        getPrice();
    }

    public Dialog onCreateDialogSingleChoice() {
        ContextThemeWrapper cw = new ContextThemeWrapper(this, R.style.AlertDialogTheme);
        AlertDialog.Builder builder = new AlertDialog.Builder(cw);
        final CharSequence[] array = {"Single Spaced = 560 words per Page", "Double Spaced = 280 words per Page"};
        builder.setItems(array, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (array[which].toString().contains("Single")) {
                    spacing = "2";
                } else {
                    spacing = "1";
                }
                tvSpacedPage.setText(array[which].toString().split("=")[0].trim());
                tvWords.setText(array[which].toString().replaceAll("[\\D]", "") + " words");
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Preferences.writeString(NewOrderActivity.this, Constants.ORDER_ID, "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
