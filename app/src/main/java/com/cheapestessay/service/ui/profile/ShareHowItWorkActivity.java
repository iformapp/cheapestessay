package com.cheapestessay.service.ui.profile;

import android.os.Bundle;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShareHowItWorkActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_how_it_work);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.how_it_works).toUpperCase());
    }

    @OnClick(R.id.ll_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Share How It Works Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open how it works screen");
        setTracker(event);
    }
}
