package com.cheapestessay.service.ui.profile;

import android.os.Bundle;
import android.view.View;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LifeTimeHowItWorkActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_silver_level)
    TextViewSFTextRegular tvSilverLevel;
    @BindView(R.id.tv_gold_level)
    TextViewSFTextRegular tvGoldLevel;
    @BindView(R.id.tv_vip_level)
    TextViewSFTextRegular tvVipLevel;

    private String medalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_time_how_it_work);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.how_it_works).toUpperCase());

        if (getIntent() != null) {
            medalName = getIntent().getStringExtra(Constants.MEDAL_LEVEL);
        }

        if (!isEmpty(medalName)) {
            if (medalName.equalsIgnoreCase("+Silver")) {
                tvSilverLevel.setVisibility(View.VISIBLE);
            } else if (medalName.equalsIgnoreCase("+Gold")) {
                tvGoldLevel.setVisibility(View.VISIBLE);
            } else if (medalName.equalsIgnoreCase("+VIP")) {
                tvVipLevel.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.ll_back)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Life Time How It Works Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open How It Works screen");
        setTracker(event);
    }
}
