package com.cheapestessay.service.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.edittext.EditTextSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NameActivity extends BaseActivity {

    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_cancel, R.id.btn_done})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                finish();
                openToLeft();
                break;
            case R.id.btn_done:
                if (isValid())
                    updateName();
                break;
        }
    }

    public String getFirstName() {
        return etFirstname.getText().toString().trim();
    }

    public String getLastName() {
        return etLastname.getText().toString().trim();
    }

    public void updateName() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().registerStep2(getFirstName(), getLastName(), getUserId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                Log.e("CreateStep2 Response", json);
                if (checkStatus(json)) {
                    UserModel userModel = new Gson().fromJson(json, UserModel.class);
                    if (userModel != null) {
                        if (checkStatus(userModel)) {
                            if (userModel.data != null) {
                                Preferences.saveUserData(NameActivity.this, userModel.data);
                                finish();
                            }
                        } else {
                            failureError(userModel.msg);
                        }
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    public boolean isValid() {
        if (isEmpty(getFirstName())) {
            validationError("Please enter first name");
            return false;
        }

        if (isEmpty(getLastName())) {
            validationError("Please enter last name");
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Signup Name Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open signup name screen");
        setTracker(event);
    }
}
