package com.cheapestessay.service.ui.profile;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.GeneralModel;
import com.cheapestessay.service.model.UserModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.ui.MainActivity;
import com.cheapestessay.service.ui.policy.PolicyActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.Utils;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.util.textview.TextViewSFTextBold;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.tv_balance)
    TextViewSFTextBold tvBalance;
    @BindView(R.id.tvTitle)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.tvInfo)
    TextViewSFDisplayRegular tvInfo;
    @BindView(R.id.tv_username)
    TextViewSFDisplayBold tvUsername;
    @BindView(R.id.tv_email)
    TextViewSFDisplayRegular tvEmail;
    @BindView(R.id.ll_profile_view)
    LinearLayout llProfileView;
    @BindView(R.id.rl_without_login)
    RelativeLayout rlWithoutLogin;
    @BindView(R.id.img_medal)
    ImageView imgMedal;
    @BindView(R.id.tv_medal)
    TextViewSFDisplayBold tvMedal;

    private Dialog dialog;
    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Profile Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open profile screen");
        setTracker(event);
        if (isLogin()) {
            userData = Preferences.getUserData(this);
            if (userData != null) {
                tvUsername.setText("Hi, " + userData.firstName);
                tvEmail.setText(userData.email);
                tvBalance.setText(Utils.priceWith$(userData.balance));
            }
            llProfileView.setVisibility(View.VISIBLE);
            rlWithoutLogin.setVisibility(View.GONE);
            getBalance();
        } else {
            tvTitle.setText(getString(R.string.profile));
            tvInfo.setText(getString(R.string.login_to_profile));
            llProfileView.setVisibility(View.GONE);
            rlWithoutLogin.setVisibility(View.VISIBLE);
        }
    }

    public void getBalance() {
        if (!isNetworkConnected())
            return;

        //showProgress();

        Call<UserModel> call = getService().getBalance(getUserId());
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel model = response.body();
                if (checkStatus(model)) {
                    userData.balance = model.data.balance;
                    userData.discountName = model.data.discountName;
                    userData.benifitValue = model.data.benifitValue;
                    userData.totalPayment = model.data.totalPayment;
                    Preferences.saveUserData(ProfileActivity.this, userData);
                    tvBalance.setText(Utils.priceWith$(userData.balance));
                    if (!isEmpty(model.data.discountName)) {
                        if (model.data.discountName.equalsIgnoreCase("+Bronze") ||
                                model.data.discountName.equalsIgnoreCase("+Blue")) {
                            imgMedal.setImageResource(R.drawable.medal_blue_fill);
                            tvMedal.setText(getString(R.string.blue));
                            tvMedal.setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.bronze_circle_round));
                        } else if (model.data.discountName.equalsIgnoreCase("+Silver")) {
                            imgMedal.setImageResource(R.drawable.medal_silver_fill);
                            tvMedal.setText(getString(R.string.silver));
                            tvMedal.setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.silver_circle_round));
                        } else if (model.data.discountName.equalsIgnoreCase("+Gold")) {
                            imgMedal.setImageResource(R.drawable.medal_gold_fill);
                            tvMedal.setText(getString(R.string.gold));
                            tvMedal.setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.gold_circle_round));
                        } else if (model.data.discountName.equalsIgnoreCase("+VIP")) {
                            imgMedal.setImageResource(R.drawable.medal_vip_fill);
                            tvMedal.setText(getString(R.string.vip));
                            tvMedal.setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.vip_circle_round));
                        }
                        imgMedal.setVisibility(View.VISIBLE);
                    } else {
                        tvMedal.setVisibility(View.GONE);
                        imgMedal.setImageResource(R.drawable.medal_blue_fill);
                        tvMedal.setText(getString(R.string.blue));
                        tvMedal.setBackground(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.bronze_circle_round));
                    }
                }
                //hideProgress();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                failureError("get balance failed");
            }
        });
    }

    @OnClick({R.id.btn_signup, R.id.btn_login, R.id.rl_edit_profile, R.id.rl_my_balance, R.id.rl_policy, R.id.rl_feedback, R.id.rl_sign_out,
            R.id.ll_profile, R.id.rl_setting, R.id.rl_15_discount, R.id.rl_lifetime_discount, R.id.tv_medal, R.id.img_medal})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(false);
                } else {
                    goToLoginSignup(false);
                }
                break;
            case R.id.btn_login:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(true);
                } else {
                    goToLoginSignup(true);
                }
                break;
            case R.id.ll_profile:
            case R.id.rl_edit_profile:
                redirectActivity(EditProfileActivity.class);
                break;
            case R.id.rl_my_balance:
                redirectActivity(BalanceActivity.class);
                break;
            case R.id.rl_policy:
                redirectActivity(PolicyActivity.class);
                break;
            case R.id.rl_feedback:
                showFeedbackDialog();
                break;
            case R.id.rl_sign_out:
                showLogoutDialog();
                break;
            case R.id.rl_setting:
                redirectActivity(SettingActivity.class);
                break;
            case R.id.img_medal:
            case R.id.tv_medal:
                redirectActivity(LifeTimeDiscountActivity.class);
                break;
            case R.id.rl_15_discount:
                redirectActivity(ShareDiscountActivity.class);
                break;
            case R.id.rl_lifetime_discount:
                redirectActivity(LifeTimeDiscountActivity.class);
                break;
        }
    }

    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(getString(R.string.logout_msg));

        tvCancel.setText(getString(R.string.no));
        tvChatnow.setText(getString(R.string.yes));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                toastMessage("Signout successfully");
                Preferences.clearPreferences(ProfileActivity.this);
                Preferences.saveUserData(ProfileActivity.this, null);
                gotoMainActivity(Constants.TAB_PROFILE);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void showFeedbackDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_feedback);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvSend = dialog.findViewById(R.id.tv_send);
        final EditText etFeedback = dialog.findViewById(R.id.et_feedback);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(etFeedback.getText().toString().trim())) {
                    sendFeedback(etFeedback.getText().toString());
                    dialog.dismiss();
                } else {
                    toastMessage("Enter message");
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void sendFeedback(String message) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().sendFeedback(getUserId(), message);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        toastMessage(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }
}
