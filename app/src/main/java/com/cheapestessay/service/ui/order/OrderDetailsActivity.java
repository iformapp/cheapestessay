package com.cheapestessay.service.ui.order;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.fragment.OrderDetailsFragment;
import com.cheapestessay.service.fragment.OrderFilesFragment;
import com.cheapestessay.service.fragment.OrderPaymentFragment;
import com.cheapestessay.service.fragment.OrderStatusFragment;
import com.cheapestessay.service.model.OrderByIdModel;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.woopra.WoopraEvent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_no_order)
    TextViewSFDisplayBold tvNoOrder;

    private OrderByIdModel.Data orderData;
    private String orderId;
    private boolean isRefresh = false;

    private String[] tabTexts = {"Status", "Details", "Files", "Payment"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            orderId = getIntent().getStringExtra(Constants.ORDER_ID);
        }
        if (orderId == null)
            return;

        getOrderById();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Order Details Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open order details screen");
        setTracker(event);
        if (isRefresh) {
            isRefresh = false;
            getOrderById();
        }

        String orderId = Preferences.readString(this, Constants.EDIT_ORDER_ID, "");
        if (!isEmpty(orderId)) {
            Preferences.writeString(this, Constants.EDIT_ORDER_ID, "");
            getOrderById();
        }
    }

    public void setupTabs() {
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_BOLD);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_REGULAR);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.textAccent));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabText();
    }

    public void getOrderById() {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<OrderByIdModel> call = getService().getOrderById(getAccessToken(), getUserId(), orderId);
        call.enqueue(new Callback<OrderByIdModel>() {
            @Override
            public void onResponse(@NonNull Call<OrderByIdModel> call, @NonNull Response<OrderByIdModel> response) {
                OrderByIdModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    viewPager.setVisibility(View.VISIBLE);
                    tvNoOrder.setVisibility(View.GONE);
                    orderData = orderModel.data;
                    tvOrderNo.setText(orderData.orderNumber);
                    setupTabs();
                } else {
                    isRefresh = true;
                    viewPager.setVisibility(View.GONE);
                    tvNoOrder.setVisibility(View.VISIBLE);
                }
                hideProgress();
            }

            @Override
            public void onFailure(@NonNull Call<OrderByIdModel> call, @NonNull Throwable t) {
                failureError(getString(R.string.order_details_not_found));
                viewPager.setVisibility(View.GONE);
                tvNoOrder.setVisibility(View.VISIBLE);
            }
        });
    }

    public OrderByIdModel.Data getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderByIdModel.Data orderData) {
        this.orderData = orderData;
    }

    private void setupTabText() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(this)
                    .inflate(i == 0 ? R.layout.custom_tab_text_select : R.layout.custom_tab_text_unselect, null);
            tv.setText(tabTexts[i]);
            if (tabLayout.getTabAt(i) != null) {
                tabLayout.getTabAt(i).setCustomView(tv);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OrderStatusFragment(), getString(R.string.status));
        adapter.addFrag(new OrderDetailsFragment(), getString(R.string.details));
        adapter.addFrag(new OrderFilesFragment(), getString(R.string.files));
        adapter.addFrag(new OrderPaymentFragment(), getString(R.string.payment));
        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(3);
    }

    @OnClick({R.id.ll_back, R.id.tv_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_chat:
                openIntercomChat();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
