package com.cheapestessay.service.ui.profile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.cheapestessay.service.R;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.ui.policy.PolicyActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.Preferences;
import com.cheapestessay.service.util.textview.TextViewSFDisplayBold;
import com.cheapestessay.service.util.textview.TextViewSFDisplayRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.setting).toUpperCase());
        tvRight.setText(getString(R.string.chat));
    }

    @OnClick({R.id.ll_back, R.id.tv_right, R.id.rl_faq, R.id.rl_policy, R.id.rl_share_app, R.id.btn_singout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                openIntercomChat();
                break;
            case R.id.rl_faq:
                redirectUsingCustomTab(getString(R.string.autologin_url, Constants.FAQS, getAccessToken()));
                break;
            case R.id.rl_policy:
                redirectActivity(PolicyActivity.class);
                break;
            case R.id.rl_share_app:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share via..."));
                break;
            case R.id.btn_singout:
                showLogoutDialog();
                break;
        }
    }

    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(getString(R.string.logout_msg));
        tvCancel.setText(getString(R.string.no));
        tvChatnow.setText(getString(R.string.yes));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                toastMessage("Signout successfully");
                Intercom.client().logout();
                Preferences.clearPreferences(SettingActivity.this);
                Preferences.saveUserData(SettingActivity.this, null);
                gotoMainActivity(Constants.TAB_PROFILE);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Setting Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open setting screen");
        setTracker(event);
    }
}
