package com.cheapestessay.service.ui.profile;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.cheapestessay.service.R;
import com.cheapestessay.service.model.ReferralLink;
import com.cheapestessay.service.ui.BaseActivity;
import com.cheapestessay.service.util.Constants;
import com.cheapestessay.service.util.button.ButtonSFTextBold;
import com.cheapestessay.service.util.textview.TextViewSFTextRegular;
import com.cheapestessay.service.woopra.WoopraEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareDiscountActivity extends BaseActivity {

    @BindView(R.id.btn_share_code)
    ButtonSFTextBold btnShareCode;
    @BindView(R.id.btn_share_link)
    ButtonSFTextBold btnShareLink;
    @BindView(R.id.tv_discount_info)
    TextViewSFTextRegular tvDiscountInfo;
    @BindView(R.id.tv_how_it_works)
    TextViewSFTextRegular tvHowItWorks;

    private String shareCode, shareLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_discount);
        ButterKnife.bind(this);

        ClickableSpan termsOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                redirectUsingCustomTab(getString(R.string.autologin_url, Constants.TERMS_USE, getAccessToken()));
            }
        };
        makeLinks(tvDiscountInfo, new String[]{"Terms of use."}, new ClickableSpan[]{termsOfUseClick});

        tvHowItWorks.setPaintFlags(tvHowItWorks.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        getReferralLink();
    }

    public void getReferralLink() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<ReferralLink> call = getService().getReferralLink(getUserId());
        call.enqueue(new Callback<ReferralLink>() {
            @Override
            public void onResponse(Call<ReferralLink> call, Response<ReferralLink> response) {
                ReferralLink model = response.body();
                if (checkStatus(model)) {
                    shareCode = model.data.couponCode;
                    shareLink = model.data.referralLink;
                    btnShareCode.setText("Share Code: " + shareCode);
                    btnShareLink.setText("CheapestEssay.com/u/" + shareCode);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ReferralLink> call, Throwable t) {
                failureError("get ReferralLink failed");
            }
        });
    }

    @OnClick({R.id.ll_back, R.id.btn_share_code, R.id.btn_share_link, R.id.tv_how_it_works})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.btn_share_code:
                if (!isEmpty(shareCode)) {
                    String textBody = "Use this code " + shareCode + " to get a discount on your first order.\n" +
                            "\n" +
                            "Place your order at https://cheapestessay.com/order.php";
                    shareReferral(textBody);
                }
                break;
            case R.id.btn_share_link:
                if (!isEmpty(shareLink)) {
                    shareReferral(shareLink);
                }
                break;
            case R.id.tv_how_it_works:
                redirectActivity(ShareHowItWorkActivity.class);
                break;
        }
    }

    private void shareReferral(String textBody) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, textBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via..."));
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WoopraEvent event = new WoopraEvent(Constants.EVENT_NAME);
        event.setProperty(Constants.EVENT_SCREEN, "Share Discount Screen");
        event.setProperty(Constants.EVENT_TITLE, "User open share discount screen");
        setTracker(event);
    }
}
